package uoc.edu.svrKpax.dao;

import java.util.List;

import uoc.edu.svrKpax.vo.Achievement;
import uoc.edu.svrKpax.vo.AchievementRequirement;
import uoc.edu.svrKpax.vo.UserAchievement;

public interface AchievementDao {
	public List<Achievement> getGameAchievements(int idGame);
	public List<UserAchievement> getUserAchievementsForGame(int idGame, int idUser);
	public List<AchievementRequirement> getAchievementRequirements(int idAchievement);
	public String addRequirementToAchievement(AchievementRequirement achReqObj);
	public Achievement getAchievement(int idAch);
	public String saveAchievement(Achievement achObj);
	public List<AchievementRequirement> getAllAchievementRequirements();
	public String deleteAchievementFromGame(Achievement achObj);
	public String deleteRequirement(AchievementRequirement reqObj);
	public String saveUserAchievement(UserAchievement uAchObj);
	public List<UserAchievement> getAllUserAchievement();
}