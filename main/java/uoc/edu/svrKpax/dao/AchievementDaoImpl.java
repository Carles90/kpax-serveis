package uoc.edu.svrKpax.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import uoc.edu.svrKpax.vo.Achievement;
import uoc.edu.svrKpax.vo.AchievementRequirement;
import uoc.edu.svrKpax.vo.UserAchievement;

@SuppressWarnings("unchecked")
public class AchievementDaoImpl extends HibernateDaoSupport implements AchievementDao {
	
	public List<Achievement> getGameAchievements(int idGame){
		DetachedCriteria criteria = DetachedCriteria.forClass(Achievement.class);
		criteria.add(Restrictions.eq("idGame", idGame));
		return (List<Achievement>)getHibernateTemplate().findByCriteria(criteria);
	}
	
	public List<UserAchievement> getUserAchievementsForGame(int idGame, int idUser){
		DetachedCriteria criteria = DetachedCriteria.forClass(UserAchievement.class);
		criteria.add(Restrictions.eq("idUser", idUser));
		
		List<UserAchievement> allUserAch = (List<UserAchievement>)getHibernateTemplate().findByCriteria(criteria);
		List<Achievement> allGameAch = getGameAchievements(idGame);
		ArrayList<UserAchievement> achUserGame = new ArrayList<UserAchievement>();
		
		for(UserAchievement ach : allUserAch){
			for(Achievement gameAch: allGameAch){
				if(ach.getIdAchievement() == gameAch.getIdAchievement()){
					achUserGame.add(ach);
					break;
				}
			}
		}
		
		return achUserGame;
	}
	
	public List<AchievementRequirement> getAchievementRequirements(int idAch){
		DetachedCriteria criteria = DetachedCriteria.forClass(AchievementRequirement.class);
		criteria.add(Restrictions.eq("idAchievement", idAch));
		return (List<AchievementRequirement>)getHibernateTemplate().findByCriteria(criteria);
	}
	
	@Override
	public String addRequirementToAchievement(AchievementRequirement achReqObj) {
		getHibernateTemplate().save(achReqObj);
		return "OK";
	}
	
	public Achievement getAchievement(int idAch){
		DetachedCriteria criteria = DetachedCriteria.forClass(Achievement.class);
		criteria.add(Restrictions.eq("idAchievement", idAch));
		List<Achievement> achList = (List<Achievement>)getHibernateTemplate().findByCriteria(criteria);
		if(achList.size() > 0){
			return achList.get(0);
		}
		return null;
	}
	
	public String saveAchievement(Achievement achObj){
		getHibernateTemplate().saveOrUpdate(achObj);
		return "OK";
	}
	
	public List<AchievementRequirement> getAllAchievementRequirements()
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(AchievementRequirement.class);
		List<AchievementRequirement> reqList = (List<AchievementRequirement>)getHibernateTemplate().findByCriteria(criteria);
		return reqList;
	}
	
	public String deleteAchievementFromGame(Achievement achObj)
	{
		getHibernateTemplate().delete(achObj);
		return "OK";
	}
	
	public String deleteRequirement(AchievementRequirement reqObj)
	{
		getHibernateTemplate().delete(reqObj);
		return "OK";
	}
	
	public String saveUserAchievement(UserAchievement uAchObj)
	{
		getHibernateTemplate().saveOrUpdate(uAchObj);
		return "OK";
	}
	
	public List<UserAchievement> getAllUserAchievement()
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(UserAchievement.class);
		List<UserAchievement> uAch = (List<UserAchievement>)getHibernateTemplate().findByCriteria(criteria);
		return uAch;
	}
}