package uoc.edu.svrKpax.dao;

import java.util.List;

import uoc.edu.svrKpax.vo.Category;
import uoc.edu.svrKpax.vo.Game;
import uoc.edu.svrKpax.vo.League;
import uoc.edu.svrKpax.vo.LeagueCategory;
import uoc.edu.svrKpax.vo.LeagueTeam;
import uoc.edu.svrKpax.vo.LeagueTeamMatch;
import uoc.edu.svrKpax.vo.LeagueTeamMembers;
import uoc.edu.svrKpax.vo.LeagueTeamPlaylistScore;
import uoc.edu.svrKpax.vo.LeagueUser;
import uoc.edu.svrKpax.vo.LeagueUserInfo;
import uoc.edu.svrKpax.vo.LeagueUserMatch;
import uoc.edu.svrKpax.vo.LeagueUserPlaylistScore;
import uoc.edu.svrKpax.vo.LeagueUserScore;
import uoc.edu.svrKpax.vo.PlaylistGame;

public interface LeagueDao {
	public List<League> getLeagues(String filter);
	public List<LeagueUser> getUserLeagues(int idUser);
	public League getLeague(int idLeague);
	public List<LeagueUser> getLeagueUsers(int idLeague);
	public List<LeagueUser> getLeagueTeamUsers(int idLeague, int idTeam);
	public List<LeagueUserInfo> getLeagueUsersInfo(int idLeague);
	public List<LeagueTeam> getLeagueTeams(int idLeague);
	public List<LeagueTeamMembers> getLeagueTeamsMembers(int idLeague);
	public LeagueTeam getLeagueTeam(int idTeam);
	public List<LeagueCategory> getLeagueCategories(int idLeague);
	public List<Category> getLeagueCategoriesInfo(int idLeague);
	public List<PlaylistGame> getLeaguePlaylist(int idLeague);
	public List<Game> getLeaguePlaylistInfo(int idLeague);
	public String saveLeague(League objLeague);
	public String addCategoryToLeague(LeagueCategory objLC);
	public String deleteCategoryFromLeague(LeagueCategory objLC);
	public String addGameToPlaylist(PlaylistGame objPlg);
	public String deleteGameFromPlaylist(PlaylistGame objPlg);
	public String removeLeague(League objLeague);
	public String removeUserFromLeague(LeagueUser objLU);
	public String saveLeagueUser(LeagueUser objLU);
	public List<LeagueUserMatch> getAllLeagueUserMatches(int idLeague);
	public List<LeagueUserScore> getAllLeagueUserScores(int idLeague);
	public LeagueUserMatch getUserMatchByPosition(int idLeague, int treeHeight, int treePos);
	public LeagueTeamMatch getTeamMatchByPosition(int idLeague, int treeHeight, int treePos);
	public List<LeagueTeamMatch> getAllLeagueTeamMatches(int idLeague);
	public List<LeagueTeamPlaylistScore> getAllLeagueTeamScores(int idLeague);
	public String deleteUserMatch(LeagueUserMatch objLUM);
	public String saveUserMatch(LeagueUserMatch objLUM);
	public String deleteLeagueUserScore(LeagueUserScore objLUS);
	public String deleteTeamMatch(LeagueTeamMatch objLTM);
	public String saveTeamMatch(LeagueTeamMatch objLTM);
	public String deleteLeagueTeamScore(LeagueTeamPlaylistScore objLTPS);
	public String saveTeam(LeagueTeam objTeam);
	public String removeTeam(LeagueTeam objTeam);
	public List<LeagueUserPlaylistScore> getLeagueSingleScoretable(int idLeague);
	public List<LeagueTeamPlaylistScore> getLeagueTeamsScoretable(int idLeague);
	public String submitLeagueSingleScore(LeagueUserScore objScore);
}
