package uoc.edu.svrKpax.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import uoc.edu.svrKpax.vo.Category;
import uoc.edu.svrKpax.vo.Game;
import uoc.edu.svrKpax.vo.League;
import uoc.edu.svrKpax.vo.LeagueCategory;
import uoc.edu.svrKpax.vo.LeagueTeam;
import uoc.edu.svrKpax.vo.LeagueTeamMatch;
import uoc.edu.svrKpax.vo.LeagueTeamMembers;
import uoc.edu.svrKpax.vo.LeagueTeamPlaylistScore;
import uoc.edu.svrKpax.vo.LeagueUser;
import uoc.edu.svrKpax.vo.LeagueUserInfo;
import uoc.edu.svrKpax.vo.LeagueUserMatch;
import uoc.edu.svrKpax.vo.LeagueUserPlaylistScore;
import uoc.edu.svrKpax.vo.LeagueUserScore;
import uoc.edu.svrKpax.vo.PlaylistGame;

@SuppressWarnings("unchecked")
public class LeagueDaoImpl extends HibernateDaoSupport implements LeagueDao 
{
	@Override
	public List<League> getLeagues(String filter) 
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(League.class);
		if(!filter.equals("all"))
		{
			criteria.add(Restrictions.eq("status", filter));
		}
		List<League> leagues = (List<League>)getHibernateTemplate().findByCriteria(criteria);
		return leagues;
	}
	
	@Override
	public List<LeagueUser> getUserLeagues(int idUser) 
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueUser.class);

		criteria.add(Restrictions.eq("idUser", idUser));
		
		List<LeagueUser> leagues = (List<LeagueUser>)getHibernateTemplate().findByCriteria(criteria);
		return leagues;
	}
	
	@Override
	public League getLeague(int idLeague)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(League.class);
		criteria.add(Restrictions.eq("idLeague", idLeague));
		List<League> leagueList = (List<League>)getHibernateTemplate().findByCriteria(criteria);
		if(leagueList.size() > 0){
			return leagueList.get(0);
		}
		return null;
	}
	
	@Override
	public List<LeagueUser> getLeagueUsers(int idLeague)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueUser.class);
		criteria.add(Restrictions.eq("idLeague", idLeague));
		List<LeagueUser> lUsers = (List<LeagueUser>)getHibernateTemplate().findByCriteria(criteria);
		return lUsers;
	}
	
	@Override
	public List<LeagueUser> getLeagueTeamUsers(int idLeague, int idTeam)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueUser.class);
		Criterion cr1 = Restrictions.eq("idLeague", idLeague);
		Criterion cr2 = Restrictions.eq("idTeam", idTeam);
		criteria.add(Restrictions.and(cr1, cr2));
		List<LeagueUser> lUsers = (List<LeagueUser>)getHibernateTemplate().findByCriteria(criteria);
		return lUsers;
	}
	
	@Override 
	public List<LeagueUserInfo> getLeagueUsersInfo(int idLeague)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueUserInfo.class);
		criteria.add(Restrictions.eq("idLeague", idLeague));
		List<LeagueUserInfo> lUsers = (List<LeagueUserInfo>)getHibernateTemplate().findByCriteria(criteria);
		return lUsers;
	}
	
	@Override
	public List<LeagueTeam> getLeagueTeams(int idLeague)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueTeam.class);
		criteria.add(Restrictions.eq("idLeague", idLeague));
		List<LeagueTeam> lTeams = (List<LeagueTeam>)getHibernateTemplate().findByCriteria(criteria);
		return lTeams;
	}
	
	@Override
	public List<LeagueTeamMembers> getLeagueTeamsMembers(int idLeague)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueTeamMembers.class);
		criteria.add(Restrictions.eq("idLeague", idLeague));
		List<LeagueTeamMembers> lTeams = (List<LeagueTeamMembers>)getHibernateTemplate().findByCriteria(criteria);
		return lTeams;
	}
	
	@Override
	public LeagueTeam getLeagueTeam(int idTeam)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueTeam.class);
		criteria.add(Restrictions.eq("idTeam", idTeam));
		List<LeagueTeam> lTeams = (List<LeagueTeam>)getHibernateTemplate().findByCriteria(criteria);
		
		if(lTeams.size() > 0)
		{
			return lTeams.get(0);
		}
		
		return null;
	}
	
	@Override
	public List<LeagueCategory> getLeagueCategories(int idLeague)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueCategory.class);
		criteria.add(Restrictions.eq("idLeague", idLeague));
		
		List<LeagueCategory> catList = (List<LeagueCategory>)getHibernateTemplate().findByCriteria(criteria);
		
		return catList;
	}
	
	@Override
	public List<Category> getLeagueCategoriesInfo(int idLeague)
	{
		org.hibernate.Session s = getSession();
		Transaction t = s.getTransaction();
		t.begin();

		List<Category> objCats = s.createSQLQuery("SELECT {ca.*} FROM category AS ca, leaguecategory AS lc WHERE lc.idCategory = ca.idCategory AND lc.idLeague = " + idLeague + "").addEntity("ca", Category.class).list();
		t.commit();
		releaseSession(s);
		
		return objCats;
	}
	
	@Override
	public List<PlaylistGame> getLeaguePlaylist(int idLeague)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(PlaylistGame.class);
		criteria.add(Restrictions.eq("idLeague", idLeague));
		criteria.addOrder(Order.asc("gameOrder"));
		List<PlaylistGame> playlist = (List<PlaylistGame>)getHibernateTemplate().findByCriteria(criteria);
		
		return playlist;
	}
	
	@Override
	public List<Game> getLeaguePlaylistInfo(int idLeague)
	{
		org.hibernate.Session s = getSession();
		Transaction t = s.getTransaction();
		t.begin();

		List<Game> objPlaylist = s.createSQLQuery("SELECT {ga.*} FROM game AS ga, playlistgames AS pl WHERE pl.idGame = ga.idGame AND pl.idLeague = " + idLeague + " ORDER BY pl.gameOrder ASC").addEntity("ga", Game.class).list();
		t.commit();
		releaseSession(s);
		
		return objPlaylist;
	}
	
	@Override
	public String saveLeague(League objLeague)
	{
		try
		{
			getHibernateTemplate().saveOrUpdate(objLeague);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "SAVING_ERROR";
		}
	}
	
	@Override
	public String addCategoryToLeague(LeagueCategory objLC)
	{
		try
		{
			getHibernateTemplate().saveOrUpdate(objLC);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "SAVING_ERROR";
		}
	}
	
	@Override
	public String deleteCategoryFromLeague(LeagueCategory objLC)
	{
		try
		{
			getHibernateTemplate().delete(objLC);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "DELETING_ERROR";
		}
	}
	
	@Override
	public String addGameToPlaylist(PlaylistGame objPlg)
	{
		try
		{
			getHibernateTemplate().saveOrUpdate(objPlg);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "SAVING_ERROR";
		}
	}
	
	@Override
	public String deleteGameFromPlaylist(PlaylistGame objPlg)
	{
		try
		{
			getHibernateTemplate().delete(objPlg);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "DELETING_ERROR";
		}
	}
	
	@Override
	public String removeLeague(League objLeague)
	{
		try
		{
			getHibernateTemplate().delete(objLeague);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "DELETING_ERROR";
		}
	}
	
	@Override
	public String removeUserFromLeague(LeagueUser objLU)
	{
		try
		{
			getHibernateTemplate().delete(objLU);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "DELETING_ERROR";
		}
	}
	
	@Override
	public String saveLeagueUser(LeagueUser objLU)
	{
		try
		{
			getHibernateTemplate().saveOrUpdate(objLU);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "SAVING_ERROR";
		}
	}
	
	@Override
	public List<LeagueUserMatch> getAllLeagueUserMatches(int idLeague)
	{
		//Obtenir tota la llista de membres de la competició
		List<LeagueUser> members = this.getLeagueUsers(idLeague);
		ArrayList<Integer> memberIds = new ArrayList<Integer>();
		
		for(LeagueUser member : members)
		{
			memberIds.add(member.getIdLeagueUser());
		}
		
		//Obtenir tots els enfrontaments on hi participen aquests membres
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueUserMatch.class);
		
		Criterion restr1 = Restrictions.in("idUser1", memberIds);
		Criterion restr2 = Restrictions.in("idUser2", memberIds);
		
		criteria.add(Restrictions.or(restr1, restr2));
		
		List<LeagueUserMatch> matchList = (List<LeagueUserMatch>)getHibernateTemplate().findByCriteria(criteria);
		
		return matchList;
	}
	
	@Override
	public List<LeagueUserScore> getAllLeagueUserScores(int idLeague)
	{
		//Obtenir tota la llista de membres de la competició
		List<LeagueUser> members = this.getLeagueUsers(idLeague);
		ArrayList<Integer> memberIds = new ArrayList<Integer>();
		
		for(LeagueUser member : members)
		{
			memberIds.add(member.getIdLeagueUser());
		}
		
		//Obtenir totes les puntuacions guardades per els membres d'aquesta lliga
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueUserScore.class);
		criteria.add(Restrictions.in("idLeagueUser", memberIds));
		
		List<LeagueUserScore> scoreList = (List<LeagueUserScore>)getHibernateTemplate().findByCriteria(criteria);
		
		return scoreList;
	}
	
	@Override
	public LeagueUserMatch getUserMatchByPosition(int idLeague, int treeHeight, int treePos)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueUserMatch.class);
		
		Criterion leagueEq = Restrictions.eq("idLeague", idLeague);
		Criterion treeHeightEq = Restrictions.eq("treeHeight", treeHeight);
		Criterion treePosEq = Restrictions.eq("treePos", treePos);
		
		criteria.add(Restrictions.and(Restrictions.and(leagueEq, treeHeightEq), treePosEq));
		
		List<LeagueUserMatch> list = (List<LeagueUserMatch>)getHibernateTemplate().findByCriteria(criteria);
		
		if(list.size() > 0)
		{
			return list.get(0);
		}
		
		return null;
	}
	
	@Override
	public LeagueTeamMatch getTeamMatchByPosition(int idLeague, int treeHeight, int treePos)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueTeamMatch.class);
		
		Criterion leagueEq = Restrictions.eq("idLeague", idLeague);
		Criterion treeHeightEq = Restrictions.eq("treeHeight", treeHeight);
		Criterion treePosEq = Restrictions.eq("treePos", treePos);
		
		criteria.add(Restrictions.and(Restrictions.and(leagueEq, treeHeightEq), treePosEq));
		
		List<LeagueTeamMatch> list = (List<LeagueTeamMatch>)getHibernateTemplate().findByCriteria(criteria);
		
		if(list.size() > 0)
		{
			return list.get(0);
		}
		
		return null;
	}
	
	@Override
	public List<LeagueTeamMatch> getAllLeagueTeamMatches(int idLeague)
	{
		//Obtenir tota la llista d'equips de la competició
		List<LeagueTeam> teams = this.getLeagueTeams(idLeague);
		ArrayList<Integer> teamIds = new ArrayList<Integer>();
		
		for(LeagueTeam team : teams)
		{
			teamIds.add(team.getIdTeam());
		}
		
		//Obtenir tots els enfrontaments on hi participen aquests membres
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueTeamMatch.class);
		
		Criterion restr1 = Restrictions.in("idTeam1", teamIds);
		Criterion restr2 = Restrictions.in("idTeam2", teamIds);
		
		criteria.add(Restrictions.or(restr1, restr2));
		
		List<LeagueTeamMatch> matchList = (List<LeagueTeamMatch>)getHibernateTemplate().findByCriteria(criteria);
		
		return matchList;
	}
	
	@Override
	public List<LeagueTeamPlaylistScore> getAllLeagueTeamScores(int idLeague)
	{
		//Obtenir tota la llista de membres de la competició
		List<LeagueTeam> teams = this.getLeagueTeams(idLeague);
		ArrayList<Integer> teamIds = new ArrayList<Integer>();
		
		for(LeagueTeam team : teams)
		{
			teamIds.add(team.getIdTeam());
		}
		
		//Obtenir totes les puntuacions guardades per els membres d'aquesta lliga
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueTeamPlaylistScore.class);
		criteria.add(Restrictions.in("idTeam", teamIds));
		
		List<LeagueTeamPlaylistScore> scoreList = (List<LeagueTeamPlaylistScore>)getHibernateTemplate().findByCriteria(criteria);
		
		return scoreList;
	}
	
	@Override
	public String deleteUserMatch(LeagueUserMatch objLUM)
	{
		try
		{
			getHibernateTemplate().delete(objLUM);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "DELETING_ERROR";
		}
	}
	
	@Override
	public String saveUserMatch(LeagueUserMatch objLUM)
	{
		try
		{
			getHibernateTemplate().saveOrUpdate(objLUM);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "SAVING_ERROR";
		}
	}
	
	@Override
	public String deleteLeagueUserScore(LeagueUserScore objLUS)
	{
		try
		{
			getHibernateTemplate().delete(objLUS);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "DELETING_ERROR";
		}		
	}
	
	@Override
	public String deleteTeamMatch(LeagueTeamMatch objLTM)
	{
		try
		{
			getHibernateTemplate().delete(objLTM);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "DELETING_ERROR";
		}
	}
	
	@Override
	public String saveTeamMatch(LeagueTeamMatch objLTM)
	{
		try
		{
			getHibernateTemplate().saveOrUpdate(objLTM);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "SAVING_ERROR";
		}
	}
	
	@Override
	public String deleteLeagueTeamScore(LeagueTeamPlaylistScore objLTPS)
	{
		try
		{
			getHibernateTemplate().delete(objLTPS);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "DELETING_ERROR";
		}		
	}
	
	@Override
	public String saveTeam(LeagueTeam objTeam)
	{
		try
		{
			getHibernateTemplate().saveOrUpdate(objTeam);
			getHibernateTemplate().refresh(objTeam); //Per a poder obtenir la ID generada per l'autoincrement de MySQL
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "SAVING_ERROR";
		}
	}
	
	@Override
	public String removeTeam(LeagueTeam objTeam)
	{
		try
		{
			getHibernateTemplate().delete(objTeam);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "DELETING_ERROR";
		}
	}
	
	@Override
	public List<LeagueUserPlaylistScore> getLeagueSingleScoretable(int idLeague)
	{
		//Obtenir tota la llista de membres de la competició
		List<LeagueUser> members = this.getLeagueUsers(idLeague);
		ArrayList<Integer> memberIds = new ArrayList<Integer>();
		
		for(LeagueUser member : members)
		{
			memberIds.add(member.getIdLeagueUser());
		}
		
		//Buscar dins de la llista de membres les puntuacions
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueUserPlaylistScore.class);
		criteria.add(Restrictions.in("idLeagueUser", memberIds));
		List<LeagueUserPlaylistScore> scoreList = (List<LeagueUserPlaylistScore>)getHibernateTemplate().findByCriteria(criteria);
		
		return scoreList;
	}
	
	@Override
	public List<LeagueTeamPlaylistScore> getLeagueTeamsScoretable(int idLeague)
	{
		//Obtenir tota la llista de membres de la competició
		List<LeagueTeam> teams = this.getLeagueTeams(idLeague);
		ArrayList<Integer> teamIds = new ArrayList<Integer>();
		
		for(LeagueTeam team : teams)
		{
			teamIds.add(team.getIdTeam());
		}
		
		//Buscar dins de la llista de membres les puntuacions
		DetachedCriteria criteria = DetachedCriteria.forClass(LeagueTeamPlaylistScore.class);
		criteria.add(Restrictions.in("idTeam", teamIds));
		List<LeagueTeamPlaylistScore> scoreList = (List<LeagueTeamPlaylistScore>)getHibernateTemplate().findByCriteria(criteria);
		
		return scoreList;
	}
	
	@Override
	public String submitLeagueSingleScore(LeagueUserScore objScore)
	{
		try
		{
			getHibernateTemplate().saveOrUpdate(objScore);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "SAVING_ERROR";
		}
	}
}
