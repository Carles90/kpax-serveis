package uoc.edu.svrKpax.dao;

import java.util.List;

import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import uoc.edu.svrKpax.vo.Game;
import uoc.edu.svrKpax.vo.GameInstance;
import uoc.edu.svrKpax.vo.GameUserBelonging;

@SuppressWarnings("unchecked")
public class GameInstanceDaoImpl extends HibernateDaoSupport implements
		GameInstanceDao {

	@Override
	public List<Game> getAllGames() {
		return getHibernateTemplate().find("from GameInstance");
	}

	@Override
	public GameInstance getGame(int idGame) {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(GameInstance.class);
		criteria.add(Restrictions.eq("idGame", idGame));
		return (GameInstance) DataAccessUtils
				.uniqueResult(getHibernateTemplate().findByCriteria(criteria));
	}

	@Override
	public void addGameInstance(GameInstance objGame) {
		getHibernateTemplate().saveOrUpdate(objGame);
	}

	@Override
	public void delGameInstance(GameInstance objGame) {
		getHibernateTemplate().delete(objGame);
	}

	@Override
	public GameInstance getInstanceUser(int userId, int idGame) {
		org.hibernate.Session s = getSession();
		Transaction t = s.getTransaction();
		t.begin();

		List<GameInstance> gameinstance = s
				.createSQLQuery(
						"select {gi.*} from GameInstance as gi inner join UserGameInstance ugi on ugi.idGameInstance = gi.idGameInstance and ugi.idUser = "
								+ userId + " and gi.idGame = " + idGame)
				.addEntity("gi", GameInstance.class).list();
		t.commit();
		releaseSession(s);

		if (gameinstance.size() != 0)
			return gameinstance.get(0);
		else
			return null;
	}

	@Override
	public List<GameInstance> getAllInstanceUser(int userId) {
		org.hibernate.Session s = getSession();
		Transaction t = s.getTransaction();
		t.begin();

		List<GameInstance> gameinstance = s
				.createSQLQuery(
						"select {gi.*} from GameInstance as gi inner join UserGameInstance ugi on ugi.idGameInstance = gi.idGameInstance and ugi.idUser = "
								+ userId )
				.addEntity("gi", GameInstance.class).list();
		t.commit();
		releaseSession(s);

		return gameinstance;
	}

	@Override
	/**
	 * Afegeix la pertanyença d'un usuari a un joc, si hi ha jugat almenys una vegada
	 */
	public String addUserToGame(GameUserBelonging objGUB)
	{
		try
		{
			getHibernateTemplate().saveOrUpdate(objGUB);
			return "OK";
		}
		catch(DataAccessException e)
		{
			return "SAVING_ERROR";
		}
	}
	
	@Override
	/**
	 * Comprova si un usuari ha jugat al joc seleccionat alguna vegada
	 */
	public GameUserBelonging getUserPersistence(int idUser, int idGame)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(GameUserBelonging.class);
		
		Criterion cr1 = Restrictions.eq("idUser", idUser);
		Criterion cr2 = Restrictions.eq("idGame", idGame);
		
		criteria.add(Restrictions.and(cr1, cr2));
		
		List<GameUserBelonging> gubList = (List<GameUserBelonging>)getHibernateTemplate().findByCriteria(criteria);
		
		if(gubList.size() != 0)
		{
			return gubList.get(0);
		}
		
		return null;
	}
}
