package uoc.edu.svrKpax.bussines;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import uoc.edu.svrKpax.dao.LeagueDao;
import uoc.edu.svrKpax.vo.Category;
import uoc.edu.svrKpax.vo.Game;
import uoc.edu.svrKpax.vo.League;
import uoc.edu.svrKpax.vo.LeagueCategory;
import uoc.edu.svrKpax.vo.LeagueTeam;
import uoc.edu.svrKpax.vo.LeagueTeamMatch;
import uoc.edu.svrKpax.vo.LeagueTeamMembers;
import uoc.edu.svrKpax.vo.LeagueTeamPlaylistScore;
import uoc.edu.svrKpax.vo.LeagueUser;
import uoc.edu.svrKpax.vo.LeagueUserInfo;
import uoc.edu.svrKpax.vo.LeagueUserMatch;
import uoc.edu.svrKpax.vo.LeagueUserPlaylistScore;
import uoc.edu.svrKpax.vo.LeagueUserScore;
import uoc.edu.svrKpax.vo.PlaylistGame;
import uoc.edu.svrKpax.vo.User;

public class LeagueBOImp implements LeagueBO {
	private SessionBO sBo;
	private LeagueDao leagueDao;
	private GameBO gBo;
	private GameInstanceBO iBo;
	
	@Override
	public List<League> getLeagues(String filter) 
	{
		return leagueDao.getLeagues(filter);
	}
	
	@Override
	public List<LeagueUser> getUserLeagues(String campusSession)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getUserLeagues(objUser.getIdUser());
		}
		
		return null;
	}
	
	@Override
	public League getLeague(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getLeague(idLeague);
		}
		
		return null;
	}
	
	@Override
	public List<LeagueUser> getLeagueUsers(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getLeagueUsers(idLeague);
		}
		
		return null;
	}
	
	@Override
	public List<LeagueUserInfo> getLeagueUsersInfo(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getLeagueUsersInfo(idLeague);
		}
		
		return null;		
	}
	
	@Override
	public List<LeagueTeamMembers> getLeagueTeams(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getLeagueTeamsMembers(idLeague);
		}
		
		return null;
	}
	
	@Override
	public List<Category> getLeagueCategories(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getLeagueCategoriesInfo(idLeague);
		}
		
		return null;
	}
	
	@Override
	public List<Game> getLeaguePlaylist(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getLeaguePlaylistInfo(idLeague);
		}
		
		return null;
	}
	
	@Override
	public int getLeagueUser(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Obtenir els membres de la competici�
			List<LeagueUser> members = leagueDao.getLeagueUsers(idLeague);
			for(LeagueUser member : members)
			{
				if(member.getIdUser() == objUser.getIdUser())
				{
					return member.getIdLeagueUser();
				}
			}
		}
		
		return -1;
	}
	
	@Override
	public String addLeague(String campusSession, int idLeague, String title, String description, long start, long end, String scoretype, String distribution, int maxusers, int maxgroups, int maxusergroup, String allowteams)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = new League();
			
			objLeague.setIdLeague(idLeague);
			objLeague.setIdOwner(objUser.getIdUser());
			objLeague.setStart(new Date(start * 1000));
			objLeague.setEnd(new Date(end * 1000));
			objLeague.setStatus("waiting");
			objLeague.setScoreType(scoretype);
			objLeague.setDistribution(distribution);
			objLeague.setMaxUsers(maxusers);
			objLeague.setMaxGroups(maxgroups);
			objLeague.setMaxUsersGroup(maxusergroup);
			objLeague.setTitle(title);
			objLeague.setDescription(description);
			objLeague.setNews("");
			objLeague.setAllowTeamCreation(allowteams);
			
			return leagueDao.saveLeague(objLeague);
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String editLeague(String campusSession, int idLeague, String title, String description, long start, long end, String scoretype)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = leagueDao.getLeague(idLeague);
			
			if(objLeague == null)
			{
				return "LEAGUE_NOT_EXISTS";
			}
			
			//Si la competici� ha acabat ja no es pot editar
			if(objLeague.getStatus().equals("finalized"))
			{
				return "LEAGUE_FINALIZED";
			}
			
			objLeague.setTitle(title);
			objLeague.setDescription(description);
			objLeague.setStart(new Date(start * 1000));
			objLeague.setEnd(new Date(end * 1000));
			
			if(objLeague.getStatus().equals("waiting"))
			{
				objLeague.setScoreType(scoretype);
			}
			
			return leagueDao.saveLeague(objLeague);
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String editNews(String campusSession, int idLeague, String news)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = leagueDao.getLeague(idLeague);
			
			if(objLeague == null)
			{
				return "LEAGUE_NOT_EXISTS";
			}
			
			objLeague.setNews(news);
			
			return leagueDao.saveLeague(objLeague);
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String addCategoryToLeague(String campusSession, int idLeague, int idCategory)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = getLeague(campusSession, idLeague);
			
			//Comprovar que la lliga existeix
			if(objLeague == null)
			{
				return "IVALID_LEAGUE";
			}
			
			//Crear nova categoria
			LeagueCategory objLC = new LeagueCategory();
			objLC.setIdLeague(idLeague);
			objLC.setIdCategory(idCategory);
			
			return leagueDao.addCategoryToLeague(objLC);
		}
		
		return "VALIDATION_ERROR";
	}
	
	public String clearLeagueCategories(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			List<LeagueCategory> catList = leagueDao.getLeagueCategories(idLeague);
			
			for(LeagueCategory catObj : catList)
			{
				leagueDao.deleteCategoryFromLeague(catObj);
			}
			
			return "OK";
		}
		
		return "VALIDATION_ERROR";
	}
	
	public String addGameToPlaylist(String campusSession, int idLeague, int idGame, int order)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = getLeague(campusSession, idLeague);
			
			//Comprovar que la lliga existeix
			if(objLeague == null)
			{
				return "IVALID_LEAGUE";
			}
			
			PlaylistGame objPlg = new PlaylistGame();
			objPlg.setIdGame(idGame);
			objPlg.setIdLeague(idLeague);
			objPlg.setGameOrder(order);
			objPlg.setAutomaticallyAdded("0");
			
			return leagueDao.addGameToPlaylist(objPlg);
		}
		
		return "VALIDATION_ERROR";
	}
	
	public String clearPlaylist(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			List<PlaylistGame> playList = leagueDao.getLeaguePlaylist(idLeague);
			
			for(PlaylistGame plObj : playList)
			{
				leagueDao.deleteGameFromPlaylist(plObj);
			}
			
			return "OK";
		}
		
		return "VALIDATION_ERROR";
	}
	
	public String removeLeague(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = getLeague(campusSession, idLeague);
			
			//Comprovar que la lliga existeix
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			return leagueDao.removeLeague(objLeague);
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public int getUserBySession(String campusSession)
	{
		return sBo.validateSession(campusSession).getIdUser();
	}
	
	@Override
	public String clearLeagueTree(League objLeague)
	{
		String cleaningResponse = "OK";
		
		//Comprovar si el tipus de puntuaci� funciona per arbre d'enfrontaments
		if(objLeague.getScoreType().equals("tree"))
		{
			//Decidir si �s un arbre d'usuaris o d'equips
			if(objLeague.getDistribution().equals("single"))
			{
				List<LeagueUserMatch> matchList = leagueDao.getAllLeagueUserMatches(objLeague.getIdLeague());
				//Eliminar tots els enfrontaments de l'arbre
				for(LeagueUserMatch match : matchList)
				{
					String tempResponse = leagueDao.deleteUserMatch(match);
					if(!tempResponse.equals("OK"))
					{
						cleaningResponse = tempResponse;
					}
				}
			}
			else
			{
				List<LeagueTeamMatch> matchList = leagueDao.getAllLeagueTeamMatches(objLeague.getIdLeague());
				//Eliminar tots els enfrontaments de l'arbre
				for(LeagueTeamMatch match : matchList)
				{
					String tempResponse = leagueDao.deleteTeamMatch(match);
					if(!tempResponse.equals("OK"))
					{
						cleaningResponse = tempResponse;
					}
				}
			}
		}
		else
		{
			return "INVALID_SCORETYPE";
		}
		
		return cleaningResponse;
	}
	
	@Override
	public String buildLeagueUserTree(League objLeague, List<LeagueUser> members)
	{
		int count = members.size();
		//Calcular la profunditat m�xima de l'arbre
		int treeHeight = (int) Math.ceil(Math.log(count)/Math.log(2));
		
		//En cas de que el nombre d'usuaris no sigui pot�ncia de 2 s'ha de
		//calcular quants hi haur� a la �ltima fila
		int lastFloorMax = (int) Math.pow(2, treeHeight);
		int lastFloor = (2 * count - lastFloorMax);
		
		//Barrejar aleat�riament la llista de membres
		Collections.shuffle(members);
		
		//Col�locar usuaris a l'arbre a la posici� correcta
		int penultimateFirstPos = (int) Math.ceil((double)lastFloor / (double)4);
		int penultimateIter = 0;
		
		for(int i = 0; i < (count - 1); i+=2)
		{
			int treeHeightPos;
			int treePos;
			
			if(i < lastFloor)
			{
				treeHeightPos = treeHeight;
				treePos = i / 2;
			}
			else
			{
				treeHeightPos = treeHeight - 1;
				treePos = penultimateFirstPos + penultimateIter;
				penultimateIter++;
			}

			LeagueUserMatch objLUM = new LeagueUserMatch();
			objLUM.setIdLeague(objLeague.getIdLeague());
			objLUM.setIdUser1(members.get(i).getIdLeagueUser());
			objLUM.setIdUser2(members.get(i + 1).getIdLeagueUser());
			objLUM.setTreeHeight(treeHeightPos);
			objLUM.setTreePos(treePos);
			objLUM.setWinner(null);
			objLUM.setScoreUser1(0);
			objLUM.setScoreUser2(0);
			objLUM.setStatus(0);
			
			leagueDao.saveUserMatch(objLUM);
		}
		
		//Si el nombre de participants �s imparell, sobrar� un usuari a la fulla dreta
		//que haur� d'esperar a un contrincant
		
		if((count % 2) == 1)
		{
			LeagueUserMatch objLUM = new LeagueUserMatch();
			objLUM.setIdLeague(objLeague.getIdLeague());
			objLUM.setIdUser1(null);
			objLUM.setIdUser2(members.get(count - 1).getIdLeagueUser());
			objLUM.setTreeHeight(treeHeight - 1);
			objLUM.setTreePos(penultimateFirstPos - 1);
			objLUM.setWinner(null);
			objLUM.setScoreUser1(0);
			objLUM.setScoreUser2(0);
			objLUM.setStatus(0);
			
			leagueDao.saveUserMatch(objLUM);
		}
		
		//Tancar la llista de reproducci� de jocs segons el tamany de l'arbre
		this.makeLoopPlaylist(objLeague, treeHeight);
		
		return "OK";
	}
	
	@Override
	public String buildLeagueTeamTree(League objLeague, List<LeagueTeam> teams)
	{
		int count = teams.size();
		//Calcular la profunditat m�xima de l'arbre
		int treeHeight = (int) Math.ceil(Math.log(count)/Math.log(2));
		
		//En cas de que el nombre d'equips no sigui pot�ncia de 2 s'ha de
		//calcular quants hi haur� a la �ltima fila
		int lastFloorMax = (int) Math.pow(2, treeHeight);
		int lastFloor = (2 * count - lastFloorMax);
		
		//Barrejar aleat�riament la llista d'equips
		Collections.shuffle(teams);
		
		//Col�locar equips a l'arbre a la posici� correcta
		int penultimateFirstPos = (int) Math.ceil((double)lastFloor / (double)4);
		int penultimateIter = 0;
		
		for(int i = 0; i < (count - 1); i+=2)
		{
			int treeHeightPos;
			int treePos;
			
			if(i < lastFloor)
			{
				treeHeightPos = treeHeight;
				treePos = i / 2;
			}
			else
			{
				treeHeightPos = treeHeight - 1;
				treePos = penultimateFirstPos + penultimateIter;
				penultimateIter++;
			}
			
			LeagueTeamMatch objLTM = new LeagueTeamMatch();
			objLTM.setIdLeague(objLeague.getIdLeague());
			objLTM.setIdTeam1(teams.get(i).getIdTeam());
			objLTM.setIdTeam2(teams.get(i + 1).getIdTeam());
			objLTM.setTreeHeight(treeHeightPos);
			objLTM.setTreePos(treePos);
			objLTM.setWinner(null);
			objLTM.setScoreTeam1(0);
			objLTM.setScoreTeam2(0);
			objLTM.setStatus(0);
			
			leagueDao.saveTeamMatch(objLTM);
		}
		
		//Si el nombre d'equips �s imparell, sobrar� un equips a la fulla dreta
		//que haur� d'esperar a un contrincant
		if((count % 2) == 1)
		{
			LeagueTeamMatch objLTM = new LeagueTeamMatch();
			objLTM.setIdLeague(objLeague.getIdLeague());
			objLTM.setIdTeam1(null);
			objLTM.setIdTeam2(teams.get(count - 1).getIdTeam());
			objLTM.setTreeHeight(treeHeight - 1);
			objLTM.setTreePos(penultimateFirstPos - 1);
			objLTM.setWinner(null);
			objLTM.setScoreTeam1(0);
			objLTM.setScoreTeam2(0);
			objLTM.setStatus(0);
			
			leagueDao.saveTeamMatch(objLTM);
		}
		
		//Tancar la llista de reproducci� de jocs segons el tamany de l'arbre
		this.makeLoopPlaylist(objLeague, treeHeight);
		
		return "OK";
	}
	
	@Override
	public String clearLeagueScoreTable(League objLeague)
	{
		String cleaningResponse = "OK";
		
		//Eliminar sempre les llistes de puntuacions dels usuaris (en un arbre per equips es guardaran aix� tamb�)
		List<LeagueUserScore> scoreList = leagueDao.getAllLeagueUserScores(objLeague.getIdLeague());
		//Eliminar totes les puntuacions de la taula
		for(LeagueUserScore score : scoreList)
		{
			String tempResponse = leagueDao.deleteLeagueUserScore(score);
			if(!tempResponse.equals("OK"))
			{
				cleaningResponse = tempResponse;
			}
		}
			
		if(objLeague.getDistribution().equals("teams"))
		{
			List<LeagueTeamPlaylistScore> teamScoreList = leagueDao.getAllLeagueTeamScores(objLeague.getIdLeague());
			//Eliminar totes les puntuacions de la taula
			for(LeagueTeamPlaylistScore score : teamScoreList)
			{
				String tempResponse = leagueDao.deleteLeagueTeamScore(score);
				if(!tempResponse.equals("OK"))
				{
					cleaningResponse = tempResponse;
				}
			}
		}
		
		return cleaningResponse;
	}
	
	@Override
	public void makeLoopPlaylist(League objLeague, int numGames)
	{
		if(objLeague.getScoreType().equals("tree"))
		{
			//Obtenir la llista de jocs inicial
			List<PlaylistGame> playlist = leagueDao.getLeaguePlaylist(objLeague.getIdLeague());
			
			int iter = 0;
			int totalAdded = playlist.size();
			
			//Emplenar la nova llista copiant els jocs en forma de bucle
			//fins arribar a la mida de l'arbre
			while(totalAdded < numGames)
			{
				PlaylistGame originGame = playlist.get(iter);
				PlaylistGame tempGame = new PlaylistGame();
				
				tempGame.setIdGame(originGame.getIdGame());
				tempGame.setIdLeague(originGame.getIdLeague());
				tempGame.setGameOrder(playlist.size());
				tempGame.setAutomaticallyAdded("1");
				
				leagueDao.addGameToPlaylist(tempGame);
				
				iter = (iter + 1) % playlist.size();
				totalAdded++;
			}
		}
	}
	
	@Override 
	public void deleteLoopPlaylist(League objLeague)
	{
		if(objLeague.getScoreType().equals("tree"))
		{
			//Obtenir la llista de jocs inicial
			List<PlaylistGame> playlist = leagueDao.getLeaguePlaylist(objLeague.getIdLeague());
			
			//Eliminar tots els que s'hagin afegit anteriorment marcats com a bucle
			for(PlaylistGame tempGame : playlist)
			{
				if(tempGame.getAutomaticallyAdded().equals("1"))
				{
					leagueDao.deleteGameFromPlaylist(tempGame);
				}
			}
		}
	}
	
	@Override
	public String leaveLeague(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = getLeague(campusSession, idLeague);
			
			//Comprovar que la lliga existeix
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la lliga estigui en estat d'espera d'usuaris
			if(!objLeague.getStatus().equals("waiting"))
			{
				return "SUBSCRIPTION_CLOSED";
			}
			
			//Comprovar que l'usuari sigui membre de la lliga
			List<LeagueUser> leagueUsers = this.getLeagueUsers(campusSession, idLeague);
			
			for(LeagueUser lUser : leagueUsers)
			{
				if(lUser.getIdUser() == objUser.getIdUser())
				{
					//Si l'usuari es troba a la llista, eliminar la seva participaci� a la lliga
					leagueDao.removeUserFromLeague(lUser);
					
					//Si l'usuari era el propietari d'un equip, eliminar-lo
					if(objLeague.getDistribution().equals("teams"))
					{
						List<LeagueTeam> lTeams = leagueDao.getLeagueTeams(idLeague);
						for(LeagueTeam lTeam : lTeams)
						{
							if(lTeam.getIdOwner() != null)
							{
								if(lTeam.getIdOwner().intValue() == objUser.getIdUser())
								{
									leagueDao.removeTeam(lTeam);
								}
							}
						}
					}
					
					return "OK";
				}
			}
			
			//Si l'usuari no es troba a la llista de membres, �s que no pertany a la competici�
			return "NOT_A_MEMBER";
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String joinLeague(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = getLeague(campusSession, idLeague);
			
			//Comprovar que la lliga existeix
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la lliga estigui en estat d'espera d'usuaris
			if(!objLeague.getStatus().equals("waiting"))
			{
				return "SUBSCRIPTION_CLOSED";
			}
			
			//Comprovar que l'usuari no sigui membre de la lliga
			List<LeagueUser> leagueUsers = this.getLeagueUsers(campusSession, idLeague);
			
			for(LeagueUser lUser : leagueUsers)
			{
				if(lUser.getIdUser() == objUser.getIdUser())
				{
					//Si l'usuari es troba a la llista vol dir que ja era membre
					return "ALREADY_A_MEMBER";
				}
			}
			
			//Comprovar que la lliga no estigui plena
			if(objLeague.getMaxUsers() > 0)
			{
				if(leagueUsers.size() >= objLeague.getMaxUsers())
				{
					return "LEAGUE_IS_FULL";
				}
			}
			
			//Guardar l'usuari a la llista de membres
			LeagueUser objLU = new LeagueUser();
			objLU.setIdLeague(idLeague);
			objLU.setIdTeam(null);
			objLU.setIdUser(objUser.getIdUser());
			
			return leagueDao.saveLeagueUser(objLU);
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String lockLeague(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = getLeague(campusSession, idLeague);
			
			//Comprovar que la lliga existeix
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la lliga estigui encara desbloquejada
			if(!objLeague.getStatus().equals("waiting"))
			{
				return "STATUS_ERROR";
			}
			
			//Comprovar que no hi hagi cap equip buit si la competici� funciona per equips
			if(objLeague.getDistribution().equals("teams"))
			{
				List<LeagueTeamMembers> teams = leagueDao.getLeagueTeamsMembers(idLeague);
				for(LeagueTeamMembers team : teams)
				{
					if(team.getMembers() == 0)
					{
						return "EMPTY_TEAMS";
					}
				}
			}
			
			//Comprovar que la competici� tingui algun joc a la seva llista de reproducci�
			List<PlaylistGame> playlist = leagueDao.getLeaguePlaylist(idLeague);
			if(playlist.size() == 0)
			{
				return "EMPTY_PLAYLIST";
			}
			
			//En el cas de que la competici� sigui un arbre d'enfrontaments, generar l'arbre.
			if(objLeague.getScoreType().equals("tree"))
			{
				//Primer netejar l'arbre si ja existia
				String treeCleanResponse = this.clearLeagueTree(objLeague);
				if(!treeCleanResponse.equals("OK"))
				{
					return "TREE_CLEANING_ERROR";
				}
				
				String buildResponse = "OK";
				
				if(objLeague.getDistribution().equals("single"))
				{
					List<LeagueUser> members = leagueDao.getLeagueUsers(idLeague);
					buildResponse = this.buildLeagueUserTree(objLeague, members);
					if(!buildResponse.equals("OK"))
					{
						return "TREE_BUILDING_ERROR";
					}
				}
				else if(objLeague.getDistribution().equals("teams"))
				{
					List<LeagueTeam> teams = leagueDao.getLeagueTeams(idLeague);
					buildResponse = this.buildLeagueTeamTree(objLeague, teams);
					if(!buildResponse.equals("OK"))
					{
						return "TREE_BUILDING_ERROR";
					}
				}
			}
			
			objLeague.setStatus("running");
			
			return leagueDao.saveLeague(objLeague);
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String unlockLeague(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = getLeague(campusSession, idLeague);
			
			//Comprovar que la lliga existeix
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la lliga estigui bloquejada
			if(!objLeague.getStatus().equals("running"))
			{
				return "STATUS_ERROR";
			}
			
			//Comprovar que la lliga no hagi comen�at ja
			if(objLeague.getStart().getTime() <= (new Date()).getTime())
			{
				return "LEAGUE_HAS_STARTED";
			}
			
			//En el cas de que la competici� sigui un arbre d'enfrontaments, eliminar l'arbre.
			if(objLeague.getScoreType().equals("tree"))
			{
				//Primer netejar l'arbre si ja existia
				String cleanResponse = this.clearLeagueTree(objLeague);
				if(!cleanResponse.equals("OK"))
				{
					return "CLEANING_ERROR";
				}
			}
			
			//Tornar a eliminar els jocs de la llista de reproducci� que no estaven inicialment
			this.deleteLoopPlaylist(objLeague);
			
			objLeague.setStatus("waiting");
			
			return leagueDao.saveLeague(objLeague);
		}
		
		return "VALIDATION_ERROR";
	}
	
	public String createTeam(String campusSession, int idLeague, String name, String password, int createdByAdmin)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			League objLeague = getLeague(campusSession, idLeague);
			
			//Comprovar que la lliga existeix
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("waiting"))
			{
				return "LEAGUE_CLOSED";
			}
			
			//Comprovar que no s'hagi excedit el n�mero m�xim d'equips
			List<LeagueTeam> leagueTeams = leagueDao.getLeagueTeams(idLeague);
			if(leagueTeams.size() >= objLeague.getMaxGroups() && objLeague.getMaxGroups() != 0)
			{
				return "TEAM_LIMIT_EXCEEDED";
			}
			
			//Seleccionar l'objecte de membre que representa a aquest usuari
			LeagueUser myLeagueUser = null;
			List<LeagueUser> leagueUsers = leagueDao.getLeagueUsers(idLeague);
			for(LeagueUser leagueUser : leagueUsers)
			{
				if(leagueUser.getIdUser() == objUser.getIdUser())
				{
					myLeagueUser = leagueUser;
				}
			}
			
			if(myLeagueUser == null && createdByAdmin == 0)
			{
				return "VALIDATION_ERROR";
			}
			
			//Crear l'equip
			LeagueTeam objTeam = new LeagueTeam();
			objTeam.setIdLeague(idLeague);
			if(createdByAdmin == 1)
			{
				objTeam.setIdOwner(null);
			}
			else
			{
				objTeam.setIdOwner(objUser.getIdUser());
			}
			objTeam.setTeamName(name);
			objTeam.setTeamPassword(password);
			
			String creatingResult = leagueDao.saveTeam(objTeam);
			
			if(!creatingResult.equals("OK"))
			{
				return creatingResult;
			}
			
			//Unir a l'usuari a aquest equip si no �s l'administrador
			if(createdByAdmin == 0)
			{
				myLeagueUser.setIdTeam(objTeam.getIdTeam());
				return leagueDao.saveLeagueUser(myLeagueUser);
			}
			else
			{
				return creatingResult;
			}
		}
		
		return "VALIDATION_ERROR";
	}
	
	public String joinToTeam(String campusSession, int idTeam, String passwordHash)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Comprovar que l'equip existeixi
			LeagueTeam objTeam = leagueDao.getLeagueTeam(idTeam);
			
			if(objTeam == null)
			{
				return "INVALID_TEAM";
			}
			
			//Comprovar que la competici� existeixi
			League objLeague = leagueDao.getLeague(objTeam.getIdLeague());
			
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("waiting"))
			{
				return "LEAGUE_CLOSED";
			}
			
			//Comprovar que la contrasenya sigui correcta
			if(!objTeam.getTeamPassword().equals(""))
			{
				if(!objTeam.getTeamPassword().equals(passwordHash))
				{
					return "INVALID_PASSWORD";
				}
			}
			
			List<LeagueUser> members = leagueDao.getLeagueUsers(objTeam.getIdLeague());
			LeagueUser objMe = null;
			int teamMembers = 0;
			
			//Comprovar que l'usuari sigui membre de la competici� i comptar
			//quants membres hi ha a l'equip
			for(LeagueUser member : members)
			{
				if(member.getIdUser() == objUser.getIdUser())
				{
					objMe = member;
					
					//Comprovar si l'usuari ja es troba en un equip
					if(member.getIdTeam() != null)
					{
						return "ALREADY_ON_A_TEAM";
					}
				}
				
				if(member.getIdTeam() != null)
				{
					if(member.getIdTeam().intValue() == objTeam.getIdTeam())
					{
						teamMembers++;
					}
				}
			}
			
			if(objMe == null)
			{
				return "NOT_A_MEMBER";
			}
			
			if(objLeague.getMaxUsersGroup() != 0 && objLeague.getMaxUsersGroup() <= teamMembers)
			{
				return "TEAM_IS_FULL";
			}
			
			//Unir a l'equip
			objMe.setIdTeam(idTeam);
			return leagueDao.saveLeagueUser(objMe);
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String leaveTeam(String campusSession, int idTeam)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Comprovar que l'equip existeixi
			LeagueTeam objTeam = leagueDao.getLeagueTeam(idTeam);
			
			if(objTeam == null)
			{
				return "INVALID_TEAM";
			}
			
			//Comprovar que la competici� existeixi
			League objLeague = leagueDao.getLeague(objTeam.getIdLeague());
			
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("waiting"))
			{
				return "LEAGUE_CLOSED";
			}
			
			List<LeagueUser> members = leagueDao.getLeagueUsers(objTeam.getIdLeague());
			LeagueUser objMe = null;
			
			//Comprovar que l'usuari sigui membre de l'equip
			for(LeagueUser member : members)
			{
				if(member.getIdUser() == objUser.getIdUser())
				{
					objMe = member;
					
					if(member.getIdTeam() != idTeam)
					{
						return "NOT_IN_THIS_TEAM";
					}
				}
			}
			
			if(objMe == null)
			{
				return "NOT_A_MEMBER";
			}
			

			//Eliminar pertinen�a a l'equip
			objMe.setIdTeam(null);
			leagueDao.saveLeagueUser(objMe);
			
			//Si l'usuari era el propietari de l'equip, eliminar l'equip.
			//La pertinen�a dels altres membres desapareixer� autom�ticament
			//gr�cies al ON DELETE CASCADE del MySQL
			if(objTeam.getIdOwner() != null)
			{
				if(objTeam.getIdOwner().intValue() == objUser.getIdUser())
				{
					leagueDao.removeTeam(objTeam);
				}
			}
			
			return "OK";
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String removeTeam(String campusSession, int idTeam)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Comprovar que l'equip existeixi
			LeagueTeam objTeam = leagueDao.getLeagueTeam(idTeam);
			
			if(objTeam == null)
			{
				return "INVALID_TEAM";
			}
			
			//Comprovar que la competici� existeixi
			League objLeague = leagueDao.getLeague(objTeam.getIdLeague());
			
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("waiting"))
			{
				return "LEAGUE_CLOSED";
			}
			
			return leagueDao.removeTeam(objTeam);
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String kickUserFromTeam(String campusSession, int idTeam, int idUser)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Comprovar que l'equip existeixi
			LeagueTeam objTeam = leagueDao.getLeagueTeam(idTeam);
			
			if(objTeam == null)
			{
				return "INVALID_TEAM";
			}
			
			//Comprovar que la competici� existeixi
			League objLeague = leagueDao.getLeague(objTeam.getIdLeague());
			
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("waiting"))
			{
				return "LEAGUE_CLOSED";
			}
			
			//Comprovar que l'usuari es troba dins de l'equip seleccionat
			List<LeagueUser> members = leagueDao.getLeagueUsers(objTeam.getIdLeague());
			
			for(LeagueUser member : members)
			{
				if(member.getIdUser() == idUser)
				{
					if(member.getIdTeam() != null && member.getIdTeam().intValue() == idTeam)
					{
						member.setIdTeam(null);
						return leagueDao.saveLeagueUser(member);
					}
					else
					{
						return "NOT_IN_THIS_TEAM";
					}
				}
			}
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String assignUserToTeam(String campusSession, int idTeam, int idUser)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Comprovar que l'equip existeixi
			LeagueTeam objTeam = leagueDao.getLeagueTeam(idTeam);
			
			if(objTeam == null)
			{
				return "INVALID_TEAM";
			}
			
			//Comprovar que la competici� existeixi
			League objLeague = leagueDao.getLeague(objTeam.getIdLeague());
			
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("waiting"))
			{
				return "LEAGUE_CLOSED";
			}
			
			//Comprovar que l'usuari no es troba dins de cap equip i que
			//l'equip no estigui ple
			List<LeagueUser> members = leagueDao.getLeagueUsers(objTeam.getIdLeague());
			LeagueUser objMe = null;
			int membernum = 0;
			
			for(LeagueUser member : members)
			{
				if(member.getIdUser() == idUser)
				{
					objMe = member;
					if(member.getIdTeam() != null)
					{
						return "USER_IN_A_TEAM";
					}
				}
				
				if(member.getIdTeam() != null)
				{
					if(member.getIdTeam().intValue() == idTeam)
					{
						membernum++;
					}
				}
			}
			
			if(membernum >= objLeague.getMaxUsersGroup() && objLeague.getMaxUsersGroup() != 0)
			{
				return "TEAM_IS_FULL";
			}
			
			if(objMe != null)
			{
				objMe.setIdTeam(idTeam);
				return leagueDao.saveLeagueUser(objMe);
			}
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public LeagueTeam getTeam(String campusSession, int idTeam)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getLeagueTeam(idTeam);
		}
		
		return null;
	}
	
	@Override
	public int getUserTeam(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			List<LeagueUser> leagueUsers = this.getLeagueUsers(campusSession, idLeague);
			
			for(LeagueUser leagueUser: leagueUsers)
			{
				if(leagueUser.getIdUser() == objUser.getIdUser())
				{
					if(leagueUser.getIdTeam() != null)
					{
						return leagueUser.getIdTeam();
					}
					else
					{
						return -1; //Cap equip
					}
				}
			}
		}
		
		return -1; //Cap equip
	}
	
	@Override
	public List<LeagueUserPlaylistScore> getLeagueSingleScoretable(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getLeagueSingleScoretable(idLeague);
		}
		
		return null;
	}
	
	@Override
	public List<LeagueUserMatch> getLeagueSingleTree(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getAllLeagueUserMatches(idLeague);
		}
		
		return null;
	}
	
	@Override
	public List<LeagueTeamMatch> getLeagueTeamsTree(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getAllLeagueTeamMatches(idLeague);
		}
		
		return null;
	}
	
	@Override
	public List<LeagueTeamPlaylistScore> getLeagueTeamsScoretable(String campusSession, int idLeague)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			return leagueDao.getLeagueTeamsScoretable(idLeague);
		}
		
		return null;
	}
	
	@Override
	public String submitLeagueSingleScore(String campusSession, String uidGame, int idLeague, int score, String submitType)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Obtenci� del joc al que s'est� jugant
			Game objGame = gBo.getGame(uidGame, null);
			
			//Si el joc no existeix, acabar
			if(objGame == null)
			{
				return "INVALID_GAME";
			}
			
			//Comprovar que una inst�ncia del joc hagi estat creada pr�viament
			if(!iBo.isInstanceCreated(campusSession, uidGame))
			{
				return "INSTANCE_NOT_CREATED";
			}
			
			//Si la competici� no existeix, acabar
			League objLeague = leagueDao.getLeague(idLeague);
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("running"))
			{
				return "LEAGUE_NOT_RUNNING";
			}
			
			long now = (new Date()).getTime();
			
			if(objLeague.getStart().getTime() > now || objLeague.getEnd().getTime() < now)
			{
				return "LEAGUE_OUT_OF_TIME";
			}
			
			//Comprovar que el tipus de competici� sigui correcte
			if(!objLeague.getScoreType().equals(submitType) && !submitType.equals("check"))
			{
				return "WRONG_SCORETYPE";
			}
			
			//Comprovar que la distribuci� de la competici� sigui individual
			if(!objLeague.getDistribution().equals("single"))
			{
				return "WRONG_DISTRIBUTION";
			}
			
			//Obtenir l'identificador de l'usuari dins d'aquesta competici�
			int idLeagueUser = this.getLeagueUser(campusSession, idLeague);
			if(idLeagueUser == -1)
			{
				return "NOT_A_MEMBER";
			}
			
			//Obtenci� de la llista de reproducci� de jocs per a comprovar
			//que s'est� enviant la puntuaci� del joc que toca
			List<PlaylistGame> playlist = leagueDao.getLeaguePlaylist(idLeague);
			int currentGame = 0;
			
			List<LeagueUserPlaylistScore> scoreList = leagueDao.getLeagueSingleScoretable(idLeague);
			for(LeagueUserPlaylistScore scoreObj : scoreList)
			{
				if(scoreObj.getIdLeagueUser() == idLeagueUser)
				{
					currentGame++;
				}
			}
			
			PlaylistGame tempGame = null;
			
			for(PlaylistGame plGame : playlist)
			{
				if(plGame.getGameOrder() == currentGame)
				{
					if(plGame.getIdGame() != objGame.getIdGame())
					{
						return "INVALID_GAME_ORDER";
					}
					else
					{
						tempGame = plGame;
					}
				}
			}
			
			//No s'ha trobat el joc a la llista de reproducci�
			if(tempGame == null)
			{
				return "PLAYLIST_ERROR";
			}
			
			//Comprovar que tots els membres hagin superat la ronda de la competici�
			//abans de poder continuar
			List<LeagueUser> members = leagueDao.getLeagueUsers(idLeague);
			
			//Obtenir ID del joc anterior
			int lastPlaylistGame = 0;
			for(PlaylistGame game : playlist)
			{
				if(game.getGameOrder() == (tempGame.getGameOrder() - 1))
				{
					lastPlaylistGame = game.getIdPlaylistGame();
				}
			}
			
			int totalSubmits = 0;
			
			//Taula de puntuacions
			if(submitType.equals("scoretable"))
			{
				//La primera ronda s'ha de poder fer sempre
				if(tempGame.getGameOrder() != 0)
				{
					//Comprovar quants usuaris han obtingut una puntuaci� en el joc anterior
					for(LeagueUserPlaylistScore scoreObj : scoreList)
					{
						if(scoreObj.getIdPlaylistGame() == lastPlaylistGame)
						{
							totalSubmits++;
						}
					}
					
					//Si tots els usuaris han obtingut una puntuaci�, ja es possible participar en aquesta ronda
					if(totalSubmits < members.size())
					{
						return "WAIT_FOR_OTHER_PLAYERS";
					}
				}
			}
			//Classificat�ria
			else if(submitType.equals("knockout"))
			{
				//La primera ronda s'ha de poder fer sempre
				if(tempGame.getGameOrder() != 0)
				{
					//Comprovar quants usuaris han obtingut una puntuaci� en el joc anterior
					for(LeagueUserPlaylistScore scoreObj : scoreList)
					{
						if(scoreObj.getIdPlaylistGame() == lastPlaylistGame)
						{
							totalSubmits++;
						}
						
						//Si l'usuari estava pr�viament desclassificat, no pot participar m�s
						if(scoreObj.getIdLeagueUser() == idLeagueUser && scoreObj.getScore() == 1)
						{
							return "YOU_ARE_KNOCKED_OUT";
						}
					}
					
					//Comptar el nombre d'usuaris desclassificats anteriorment
					//ja que no fa falta que hagin incl�s una nova puntuaci�
					//en l'�ltim joc
					int totalKnockOut = 0;
					for(LeagueUserPlaylistScore scoreObj : scoreList)
					{
						if(scoreObj.getIdPlaylistGame() < lastPlaylistGame && scoreObj.getScore() == 1)
						{
							totalKnockOut++;
						}
					}
					
					//Si tots els usuaris classificats en la ronda anterior
					//no han participat, es retornar� un error
					if(totalSubmits < (members.size() - totalKnockOut))
					{
						return "WAIT_FOR_OTHER_PLAYERS";
					}
				}
			}
			
			//No hi ha cap error, afegir puntuaci� segons el tipus de competici�
			if(submitType.equals("scoretable"))
			{
				LeagueUserScore newScore = new LeagueUserScore();
				newScore.setIdLeagueUser(idLeagueUser);
				newScore.setIdPlaylistGame(tempGame.getIdPlaylistGame());
				newScore.setScore(score);
				
				//Si era la �ltima puntuaci� a incloure, donar la competici� com a finalitzada
				int totalMatches = playlist.size() * members.size();
				if((scoreList.size() + 1) >= totalMatches)
				{
					objLeague.setStatus("finalized");
					objLeague.setEnd(new Date());
					leagueDao.saveLeague(objLeague);
				}
				
				return leagueDao.submitLeagueSingleScore(newScore);
			}
			else if(submitType.equals("knockout"))
			{
				LeagueUserScore newScore = new LeagueUserScore();
				newScore.setIdLeagueUser(idLeagueUser);
				newScore.setIdPlaylistGame(tempGame.getIdPlaylistGame());
				
				//Escriure un 0 si l'usuari pot seguir jugant o b� un 1 si ha estat descalificat
				int knockout = 0;
				if(score != 0)
				{
					knockout = 1;
				}
				newScore.setScore(knockout);
				
				//Si era la ultima puntuaci� a incloure, donar la competici� com a finalitzada
				//Comptar el nombre d'usuaris classificats a la �ltima ronda i el nombre de puntuacions que s'han sotm�s en aquesta
				//Si tots els usuaris han quedat desqualificats, tamb� s'ha de finalitzar
				int totalScores = 1;
				int totalKnockout = 0;
				for(LeagueUserPlaylistScore scoreObj : scoreList)
				{
					if(scoreObj.getIdPlaylistGame() == tempGame.getIdPlaylistGame())
					{
						totalScores++;
					}
					
					if(scoreObj.getScore() == 1)
					{
						totalKnockout++;
					}
				}
				
				PlaylistGame lastGame = playlist.get(playlist.size() - 1);
				
				if(score == 1)
				{
					totalKnockout++;
				}
				
				if((totalScores >= (members.size() - totalKnockout) && tempGame.getIdPlaylistGame() == lastGame.getIdPlaylistGame()) || totalKnockout >= members.size())
				{
					objLeague.setStatus("finalized");
					objLeague.setEnd(new Date());
					leagueDao.saveLeague(objLeague);
				}
				
				return leagueDao.submitLeagueSingleScore(newScore);
			}
			else
			{
				return "OK";
			}
		}
		
		return "VALIDATION_ERROR";
	}
	
	public String submitLeagueTreeSingleScore(String campusSession, String uidGame, int idLeague, int score, String submitType)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Obtenci� del joc al que s'est� jugant
			Game objGame = gBo.getGame(uidGame, null);
			
			//Si el joc no existeix, acabar
			if(objGame == null)
			{
				return "INVALID_GAME";
			}
			
			//Comprovar que una inst�ncia del joc hagi estat creada pr�viament
			if(!iBo.isInstanceCreated(campusSession, uidGame))
			{
				return "INSTANCE_NOT_CREATED";
			}
			
			//Si la competici� no existeix, acabar
			League objLeague = leagueDao.getLeague(idLeague);
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("running"))
			{
				return "LEAGUE_NOT_RUNNING";
			}
			
			long now = (new Date()).getTime();
			
			if(objLeague.getStart().getTime() > now || objLeague.getEnd().getTime() < now)
			{
				return "LEAGUE_OUT_OF_TIME";
			}
			
			//Comprovar que el tipus de competici� sigui correcte
			if(!objLeague.getScoreType().equals(submitType) && !submitType.equals("treecheck"))
			{
				return "WRONG_SCORETYPE";
			}
			
			//Comprovar que la distribuci� de la competici� sigui individual
			if(!objLeague.getDistribution().equals("single"))
			{
				return "WRONG_DISTRIBUTION";
			}
			
			//Obtenir l'identificador de l'usuari dins d'aquesta competici�
			int idLeagueUser = this.getLeagueUser(campusSession, idLeague);
			if(idLeagueUser == -1)
			{
				return "NOT_A_MEMBER";
			}
			
			//Obtenir nivell de l'arbre al que toca incloure la puntuaci�
			int treeLevel = -1;
			List<LeagueUserMatch> matches = leagueDao.getAllLeagueUserMatches(objLeague.getIdLeague());
			LeagueUserMatch tempMatch = null;
			
			for(LeagueUserMatch match : matches)
			{
				//Encara no hi ha contrincant
				if(match.getIdUser1() == null || match.getIdUser2() == null)
				{
					continue;
				}
				
				if(match.getIdUser1().intValue() == idLeagueUser || match.getIdUser2().intValue() == idLeagueUser)
				{
					if(treeLevel == -1)
					{
						treeLevel = match.getTreeHeight();
						tempMatch = match;
					}
					else
					{
						if(match.getTreeHeight() < treeLevel)
						{
							treeLevel = match.getTreeHeight();
							tempMatch = match;
						}
					}
				}
			}
			
			if(treeLevel == -1)
			{
				return "TREE_LEVEL_ERROR";
			}
			
			//Comprovar que el joc que envia la puntuaci� sigui el que es correspon a aquest nivell de l'arbre
			List<PlaylistGame> playlist = leagueDao.getLeaguePlaylist(idLeague);
			PlaylistGame plGame = playlist.get(playlist.size() - treeLevel);
			
			if(plGame == null)
			{
				return "WRONG_GAME";
			}
			
			if(plGame.getIdGame() != objGame.getIdGame())
			{
				return "WRONG_GAME";
			}
			
			//Comprovar que no s'hagi introduit la puntuaci� pr�viament
			if(tempMatch.getStatus() == 3 || (tempMatch.getStatus() == 1 && tempMatch.getIdUser1().intValue() == idLeagueUser) || (tempMatch.getStatus() == 2 && tempMatch.getIdUser2().intValue() == idLeagueUser))
			{
				return "DUPLICATED_SCORE";
			}
			
			//Esperar que s'hagin fet tots els enfrontaments del nivell anterior
			List<LeagueUser> members = leagueDao.getLeagueUsers(idLeague);
			//Calcular la profunditat m�xima de l'arbre
			int treeHeight = (int) Math.ceil(Math.log(members.size())/Math.log(2));
			
			//El primer combat s'ha de poder fer sempre
			if(tempMatch.getTreeHeight() != treeHeight)
			{
				int lastLevel = tempMatch.getTreeHeight() + 1;
				
				//Comprovar que tots els combats del nivell anterior tinguin guanyador
				for(LeagueUserMatch match : matches)
				{
					if(match.getTreeHeight() == lastLevel)
					{
						if(match.getWinner() == null)
						{
							return "WAIT_FOR_OTHER_PLAYERS";
						}
					}
				}
			}
			
			
			//Si nom�s es volia consultar la possibilitat de guardar la puntuaci�
			//en aquest combat, acabar aqu�.
			if(submitType.equals("treecheck"))
			{
				return "OK";
			}
			
			
			//Guardar puntuaci�
			boolean endMatch = false;
			if(tempMatch.getIdUser1().intValue() == idLeagueUser)
			{
				tempMatch.setScoreUser1(score);
				
				if(tempMatch.getStatus() == 2)
				{
					endMatch = true;
				}
				else
				{
					tempMatch.setStatus(1);
				}
			}
			else if(tempMatch.getIdUser2().intValue() == idLeagueUser)
			{
				tempMatch.setScoreUser2(score);
				
				if(tempMatch.getStatus() == 1)
				{
					endMatch = true;
				}
				else
				{
					tempMatch.setStatus(2);
				}
			}
			
			//Crear nou combat a l'arbre si els dos participants han jugat
			//o actualitzar-lo si ja es trobava creat anteriorment
			if(endMatch)
			{
				tempMatch.setStatus(3);
				if(tempMatch.getScoreUser1() >= tempMatch.getScoreUser2())
				{
					tempMatch.setWinner(tempMatch.getIdUser1().intValue());
				}
				else
				{
					tempMatch.setWinner(tempMatch.getIdUser2().intValue());
				}
				
				//Generar nou enfronament si no era l'�ltim
				if(tempMatch.getTreeHeight() > 1)
				{
					//Comprovar si ja s'havia creat l'enfrontament
					int lastPos = tempMatch.getTreePos();
					int newPos = (int)Math.floor((double)lastPos / (double)2);
					LeagueUserMatch objMatch = leagueDao.getUserMatchByPosition(idLeague, treeLevel - 1, newPos);
					
					if(objMatch == null) //No existia
					{
						objMatch = new LeagueUserMatch();
						objMatch.setIdLeague(idLeague);
						objMatch.setIdUser1(null);
						objMatch.setIdUser2(null);
						objMatch.setScoreUser1(0);
						objMatch.setScoreUser2(0);
						objMatch.setStatus(0);
						objMatch.setTreeHeight(treeLevel - 1);
						objMatch.setTreePos(newPos);
						objMatch.setWinner(null);
					}
					
					//Si la posici� anterior era parell, deixar usuari a la esquerra
					//sino, deixar-lo a la dreta
					if((lastPos % 2) == 0)
					{
						objMatch.setIdUser1(tempMatch.getWinner().intValue());
					}
					else
					{
						objMatch.setIdUser2(tempMatch.getWinner().intValue());
					}
					
					//Desar nou combat
					leagueDao.saveUserMatch(objMatch);
				}
				else
				{
					//Si ja s'ha fet la final, tancar la competici�
					objLeague.setStatus("finalized");
					objLeague.setEnd(new Date());
					leagueDao.saveLeague(objLeague);
				}
			}
			
			//Desar canvis
			return leagueDao.saveUserMatch(tempMatch);
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String submitLeagueTeamScore(String campusSession, String uidGame, int idLeague, int score, String submitType)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Obtenci� del joc al que s'est� jugant
			Game objGame = gBo.getGame(uidGame, null);
			
			//Si el joc no existeix, acabar
			if(objGame == null)
			{
				return "INVALID_GAME";
			}
			
			//Comprovar que una inst�ncia del joc hagi estat creada pr�viament
			if(!iBo.isInstanceCreated(campusSession, uidGame))
			{
				return "INSTANCE_NOT_CREATED";
			}
			
			//Si la competici� no existeix, acabar
			League objLeague = leagueDao.getLeague(idLeague);
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("running"))
			{
				return "LEAGUE_NOT_RUNNING";
			}
			
			long now = (new Date()).getTime();
			
			if(objLeague.getStart().getTime() > now || objLeague.getEnd().getTime() < now)
			{
				return "LEAGUE_OUT_OF_TIME";
			}
			
			//Comprovar que el tipus de competici� sigui correcte
			if(!objLeague.getScoreType().equals(submitType) && !submitType.equals("check"))
			{
				return "WRONG_SCORETYPE";
			}
			
			//Comprovar que la distribuci� de la competici� sigui per equips
			if(!objLeague.getDistribution().equals("teams"))
			{
				return "WRONG_DISTRIBUTION";
			}
			
			//Obtenir l'identificador de l'usuari dins d'aquesta competici�
			int idLeagueUser = this.getLeagueUser(campusSession, idLeague);
			if(idLeagueUser == -1)
			{
				return "NOT_A_MEMBER";
			}
			
			//Obtenci� de la llista de reproducci� de jocs per a comprovar
			//que s'est� enviant la puntuaci� del joc que toca
			List<PlaylistGame> playlist = leagueDao.getLeaguePlaylist(idLeague);
			int currentGame = 0;
			
			List<LeagueUserPlaylistScore> scoreList = leagueDao.getLeagueSingleScoretable(idLeague);
			List<LeagueTeamPlaylistScore> scoreTeamList = leagueDao.getLeagueTeamsScoretable(idLeague);
			for(LeagueUserPlaylistScore scoreObj : scoreList)
			{
				if(scoreObj.getIdLeagueUser() == idLeagueUser)
				{
					currentGame++;
				}
			}
			
			PlaylistGame tempGame = null;
			
			for(PlaylistGame plGame : playlist)
			{
				if(plGame.getGameOrder() == currentGame)
				{
					if(plGame.getIdGame() != objGame.getIdGame())
					{
						return "INVALID_GAME_ORDER";
					}
					else
					{
						tempGame = plGame;
					}
				}
			}
			
			//No s'ha trobat el joc a la llista de reproducci�
			if(tempGame == null)
			{
				return "PLAYLIST_ERROR";
			}
			
			//Obtenci� de l'equip d'aquest jugador
			List<LeagueUser> members = leagueDao.getLeagueUsers(idLeague);
			List<LeagueTeamMembers> teams = leagueDao.getLeagueTeamsMembers(idLeague);
			
			LeagueTeamMembers objTeam = null;
			for(LeagueUser objLeagueUser : members)
			{
				if(objLeagueUser.getIdLeagueUser() == idLeagueUser)
				{	
					if(objLeagueUser.getIdTeam() != null)
					{
						for(LeagueTeamMembers tempTeam : teams)
						{
							if(tempTeam.getIdTeam() == objLeagueUser.getIdTeam().intValue())
							{
								objTeam = tempTeam;
								break;
							}
						}
					}
					break;
				}
			}
			
			if(objTeam == null)
			{
				return "TEAM_ERROR";
			}
			
			//Comptar nombre d'usuaris que estan en algun equip
			int usersOnATeam = 0;
			for(LeagueUser tempUser : members)
			{
				if(tempUser.getIdTeam() != null)
				{
					usersOnATeam++;
				}
			}
			
			//Comprovar que tots els membres hagin superat la ronda de la competici�
			//abans de poder continuar
			
			//Obtenir ID del joc anterior
			int lastPlaylistGame = 0;
			for(PlaylistGame game : playlist)
			{
				if(game.getGameOrder() == (tempGame.getGameOrder() - 1))
				{
					lastPlaylistGame = game.getIdPlaylistGame();
				}
			}
			
			int totalSubmits = 0;
			
			//Taula de puntuacions
			if(submitType.equals("scoretable"))
			{
				//La primera ronda s'ha de poder fer sempre
				if(tempGame.getGameOrder() != 0)
				{
					//Comprovar quants equips han obtingut una puntuaci� en el joc anterior
					for(LeagueUserPlaylistScore scoreObj : scoreList)
					{
						if(scoreObj.getIdPlaylistGame() == lastPlaylistGame)
						{
							totalSubmits++;
						}
					}
					
					
					//Si tots els usuaris han obtingut una puntuaci�, ja es possible participar en aquesta ronda
					if(totalSubmits < usersOnATeam)
					{
						return "WAIT_FOR_OTHER_TEAMS";
					}
				}
			}
			//Classificat�ria
			else if(submitType.equals("knockout"))
			{
				//La primera ronda s'ha de poder fer sempre
				if(tempGame.getGameOrder() != 0)
				{
					//Comprovar quants equips han obtingut una puntuaci� en el joc anterior
					for(LeagueTeamPlaylistScore scoreObj : scoreTeamList)
					{
						if(scoreObj.getIdPlaylistGame() == lastPlaylistGame)
						{
							totalSubmits += scoreObj.getSubmits();
						}
						
						//Si l'equip estava pr�viament desclassificat, no pot participar m�s
						if(scoreObj.getIdTeam() == objTeam.getIdTeam() && scoreObj.getAvgscore() == 1 && scoreObj.getIdPlaylistGame() != tempGame.getIdPlaylistGame())
						{
							return "TEAM_KNOCKED_OUT";
						}
					}
					
					//Comptar el nombre d'equips desclassificats anteriorment
					//ja que no fa falta que hagin incl�s una nova puntuaci�
					//en l'�ltim joc
					int totalKnockOut = 0;
					for(LeagueTeamPlaylistScore scoreObj : scoreTeamList)
					{
						if(scoreObj.getIdPlaylistGame() < lastPlaylistGame && scoreObj.getAvgscore() == 1)
						{
							totalKnockOut += scoreObj.getSubmits();
						}
					}
					
					//Si tots els equips classificats en la ronda anterior
					//no han participat, es retornar� un error
					if(totalSubmits < (usersOnATeam - totalKnockOut))
					{
						return "WAIT_FOR_OTHER_TEAMS";
					}
				}
			}
			
			//No hi ha cap error, afegir puntuaci� segons el tipus de competici�
			if(submitType.equals("scoretable"))
			{
				LeagueUserScore newScore = new LeagueUserScore();
				newScore.setIdLeagueUser(idLeagueUser);
				newScore.setIdPlaylistGame(tempGame.getIdPlaylistGame());
				newScore.setScore(score);
				
				//Si era la �ltima puntuaci� a incloure, donar la competici� com a finalitzada
				int totalScores = playlist.size() * usersOnATeam;
				if((scoreList.size() + 1) >= totalScores)
				{
					objLeague.setStatus("finalized");
					objLeague.setEnd(new Date());
					leagueDao.saveLeague(objLeague);
				}
				
				return leagueDao.submitLeagueSingleScore(newScore);
			}
			else if(submitType.equals("knockout"))
			{
				LeagueUserScore newScore = new LeagueUserScore();
				newScore.setIdLeagueUser(idLeagueUser);
				newScore.setIdPlaylistGame(tempGame.getIdPlaylistGame());
				
				//Escriure un 0 si l'usuari pot seguir jugant o b� un 1 si ha estat descalificat
				int knockout = 0;
				if(score != 0)
				{
					knockout = 1;
				}
				newScore.setScore(knockout);
				
				// Si era la �ltima puntuaci� a incloure o tots els equips estan descalificats, 
				// donar la competici� per finalitzada.
				int thisTeamScores = 1;
				
				for(LeagueUserPlaylistScore scoreObj : scoreList)
				{
					if(scoreObj.getIdPlaylistGame() == tempGame.getIdPlaylistGame() && objTeam.getIdTeam() == scoreObj.getIdTeam().intValue())
					{
						thisTeamScores++;
					}
				}
				
				//Comprovar n�mero de puntuacions enviades per l'equip en l'�ltim joc
				if(thisTeamScores >= objTeam.getMembers())
				{
					int totalScores = 1;
					int totalKnockout = 0;
					
					for(LeagueTeamPlaylistScore scoreObj : scoreTeamList)
					{
						if(scoreObj.getIdPlaylistGame() == tempGame.getIdPlaylistGame())
						{
							totalScores += scoreObj.getSubmits();
						}
						
						if(scoreObj.getAvgscore() == 1)
						{
							totalKnockout += scoreObj.getSubmits();
						}
					}
					
					PlaylistGame lastGame = playlist.get(playlist.size() - 1);
					
					//Comprovar si aquest equip queda descalificat o no
					int teamKnockout = score;
					
					for(LeagueUserPlaylistScore objScore : scoreList)
					{
						if(objScore.getIdTeam() != null)
						{
							if(objScore.getIdTeam().intValue() == objTeam.getIdTeam())
							{
								teamKnockout += objScore.getScore();
							}
						}
					}
					
					//Calcular mitjana (si la meitat dels usuaris estan descalificats, l'equip queda descalificat)
					teamKnockout = (int)Math.round((double)teamKnockout / (double)objTeam.getMembers());
					
					if(teamKnockout == 1)
					{
						totalKnockout++;
					}
					
					if((totalScores >= (usersOnATeam - totalKnockout) && tempGame.getIdPlaylistGame() == lastGame.getIdPlaylistGame()) || totalKnockout >= usersOnATeam)
					{
						objLeague.setStatus("finalized");
						objLeague.setEnd(new Date());
						leagueDao.saveLeague(objLeague);
					}
				}
				
				return leagueDao.submitLeagueSingleScore(newScore);
			}
			else
			{
				return "OK";
			}
		}
		
		return "VALIDATION_ERROR";
	}
	
	@Override
	public String submitLeagueTreeTeamsScore(String campusSession, String uidGame, int idLeague, int score, String submitType)
	{
		//Validar sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if(objUser != null)
		{
			//Obtenci� del joc al que s'est� jugant
			Game objGame = gBo.getGame(uidGame, null);
			
			//Si el joc no existeix, acabar
			if(objGame == null)
			{
				return "INVALID_GAME";
			}
			
			//Comprovar que una inst�ncia del joc hagi estat creada pr�viament
			if(!iBo.isInstanceCreated(campusSession, uidGame))
			{
				return "INSTANCE_NOT_CREATED";
			}
			
			//Si la competici� no existeix, acabar
			League objLeague = leagueDao.getLeague(idLeague);
			if(objLeague == null)
			{
				return "INVALID_LEAGUE";
			}
			
			//Comprovar que la competici� estigui oberta
			if(!objLeague.getStatus().equals("running"))
			{
				return "LEAGUE_NOT_RUNNING";
			}
			
			long now = (new Date()).getTime();
			
			if(objLeague.getStart().getTime() > now || objLeague.getEnd().getTime() < now)
			{
				return "LEAGUE_OUT_OF_TIME";
			}
			
			//Comprovar que el tipus de competici� sigui correcte
			if(!objLeague.getScoreType().equals(submitType) && !submitType.equals("treecheck"))
			{
				return "WRONG_SCORETYPE";
			}
			
			//Comprovar que la distribuci� de la competici� sigui individual
			if(!objLeague.getDistribution().equals("teams"))
			{
				return "WRONG_DISTRIBUTION";
			}
			
			//Obtenir l'identificador de l'usuari dins d'aquesta competici�
			int idLeagueUser = this.getLeagueUser(campusSession, idLeague);
			if(idLeagueUser == -1)
			{
				return "NOT_A_MEMBER";
			}
			
			//Obtenci� de l'equip d'aquest jugador
			List<LeagueUser> members = leagueDao.getLeagueUsers(idLeague);
			List<LeagueTeamMembers> teams = leagueDao.getLeagueTeamsMembers(idLeague);
			
			LeagueTeamMembers objTeam = null;
			for(LeagueUser objLeagueUser : members)
			{
				if(objLeagueUser.getIdLeagueUser() == idLeagueUser)
				{	
					if(objLeagueUser.getIdTeam() != null)
					{
						for(LeagueTeamMembers tempTeam : teams)
						{
							if(tempTeam.getIdTeam() == objLeagueUser.getIdTeam().intValue())
							{
								objTeam = tempTeam;
								break;
							}
						}
					}
					break;
				}
			}
			
			if(objTeam == null)
			{
				return "TEAM_ERROR";
			}
			
			//Obtenir nivell de l'arbre al que toca incloure la puntuaci�
			int treeLevel = -1;
			List<LeagueTeamMatch> matches = leagueDao.getAllLeagueTeamMatches(objLeague.getIdLeague());
			LeagueTeamMatch tempMatch = null;
			
			for(LeagueTeamMatch match : matches)
			{
				//Encara no hi ha contrincant
				if(match.getIdTeam1() == null || match.getIdTeam2() == null)
				{
					continue;
				}
				
				if(match.getIdTeam1().intValue() == objTeam.getIdTeam() || match.getIdTeam2().intValue() == objTeam.getIdTeam())
				{
					if(treeLevel == -1)
					{
						treeLevel = match.getTreeHeight();
						tempMatch = match;
					}
					else
					{
						if(match.getTreeHeight() < treeLevel)
						{
							treeLevel = match.getTreeHeight();
							tempMatch = match;
						}
					}
				}
			}
			
			if(treeLevel == -1)
			{
				return "TREE_LEVEL_ERROR";
			}
			
			//Comprovar que el joc que envia la puntuaci� sigui el que es correspon a aquest nivell de l'arbre
			List<PlaylistGame> playlist = leagueDao.getLeaguePlaylist(idLeague);
			PlaylistGame plGame = playlist.get(playlist.size() - treeLevel);
			
			if(plGame == null)
			{
				return "WRONG_GAME";
			}
			
			if(plGame.getIdGame() != objGame.getIdGame())
			{
				return "WRONG_GAME";
			}
			
			//Comprovar que no s'hagi introduit la puntuaci� pr�viament per l'equip
			if(tempMatch.getStatus() == 3 || (tempMatch.getStatus() == 1 && tempMatch.getIdTeam1().intValue() == objTeam.getIdTeam()) || (tempMatch.getStatus() == 2 && tempMatch.getIdTeam2().intValue() == objTeam.getIdTeam()))
			{
				return "DUPLICATED_TEAM_SCORE";
			}
			
			//Comprovar que aquest usuari no hagi enviat ja una puntuaci� individual
			List<LeagueUserScore> scoreList = leagueDao.getAllLeagueUserScores(idLeague);
			for(LeagueUserScore objScore : scoreList)
			{
				if(objScore.getIdPlaylistGame() == plGame.getIdPlaylistGame() && objScore.getIdLeagueUser() == idLeagueUser)
				{
					return "DUPLICATED_USER_SCORE";
				}
			}
			
			//Esperar que s'hagin fet tots els enfrontaments del nivell anterior
			//Calcular la profunditat m�xima de l'arbre
			int treeHeight = (int) Math.ceil(Math.log(members.size())/Math.log(2));
			
			//El primer combat s'ha de poder fer sempre
			if(tempMatch.getTreeHeight() != treeHeight)
			{
				int lastLevel = tempMatch.getTreeHeight() + 1;
				
				//Comprovar que tots els combats del nivell anterior tinguin guanyador
				for(LeagueTeamMatch match : matches)
				{
					if(match.getTreeHeight() == lastLevel)
					{
						if(match.getWinner() == null)
						{
							return "WAIT_FOR_OTHER_TEAMS";
						}
					}
				}
			}
			
			
			//Si nom�s es volia consultar la possibilitat de guardar la puntuaci�
			//en aquest combat, acabar aqu�.
			if(submitType.equals("treecheck"))
			{
				return "OK";
			}
			
			//Guardar la puntuaci� individual
			LeagueUserScore objSingleScore = new LeagueUserScore();
			objSingleScore.setIdLeagueUser(idLeagueUser);
			objSingleScore.setIdPlaylistGame(plGame.getIdPlaylistGame());
			objSingleScore.setScore(score);
			leagueDao.submitLeagueSingleScore(objSingleScore);
			int sentScoresByTeam = 1;
			int totalScore = score;
			
			//Si tots els usuaris de l'equip han enviat la puntuaci�, calcular la mitjana i introduir-la a l'arbre.
			for(LeagueUserScore objScore: scoreList)
			{
				if(objScore.getIdPlaylistGame() == plGame.getIdPlaylistGame())
				{
					for(LeagueUser member : members)
					{
						if(member.getIdTeam() != null)
						{
							if(objScore.getIdLeagueUser() == member.getIdLeagueUser() && member.getIdTeam() == objTeam.getIdTeam())
							{
								sentScoresByTeam++;
								totalScore += objScore.getScore();
							}
						}
					}
				}
			}
			
			//Guardar puntuaci� de l'equip a l'arbre
			if(sentScoresByTeam >= objTeam.getMembers())
			{
				boolean endMatch = false;
				int avgScore = (int)Math.round((double)totalScore / (double)sentScoresByTeam);
				
				if(tempMatch.getIdTeam1().intValue() == objTeam.getIdTeam())
				{
					tempMatch.setScoreTeam1(avgScore);
					
					if(tempMatch.getStatus() == 2)
					{
						endMatch = true;
					}
					else
					{
						tempMatch.setStatus(1);
					}
				}
				else if(tempMatch.getIdTeam2().intValue() == objTeam.getIdTeam())
				{
					tempMatch.setScoreTeam2(avgScore);
					
					if(tempMatch.getStatus() == 1)
					{
						endMatch = true;
					}
					else
					{
						tempMatch.setStatus(2);
					}
				}
				
				//Crear nou combat a l'arbre si els dos equips han jugat
				//o actualitzar-lo si ja es trobava creat anteriorment
				if(endMatch)
				{
					tempMatch.setStatus(3);
					if(tempMatch.getScoreTeam1() >= tempMatch.getScoreTeam2())
					{
						tempMatch.setWinner(tempMatch.getIdTeam1().intValue());
					}
					else
					{
						tempMatch.setWinner(tempMatch.getIdTeam2().intValue());
					}
					
					//Generar nou enfronament si no era l'�ltim
					if(tempMatch.getTreeHeight() > 1)
					{
						//Comprovar si ja s'havia creat l'enfrontament
						int lastPos = tempMatch.getTreePos();
						int newPos = (int)Math.floor((double)lastPos / (double)2);
						LeagueTeamMatch objMatch = leagueDao.getTeamMatchByPosition(idLeague, treeLevel - 1, newPos);
						
						if(objMatch == null) //No existia
						{
							objMatch = new LeagueTeamMatch();
							objMatch.setIdLeague(idLeague);
							objMatch.setIdTeam1(null);
							objMatch.setIdTeam2(null);
							objMatch.setScoreTeam1(0);
							objMatch.setScoreTeam2(0);
							objMatch.setStatus(0);
							objMatch.setTreeHeight(treeLevel - 1);
							objMatch.setTreePos(newPos);
							objMatch.setWinner(null);
						}
						
						//Si la posici� anterior era parell, deixar equip a la esquerra
						//sino, deixar-lo a la dreta
						if((lastPos % 2) == 0)
						{
							objMatch.setIdTeam1(tempMatch.getWinner().intValue());
						}
						else
						{
							objMatch.setIdTeam2(tempMatch.getWinner().intValue());
						}
						
						//Desar nou combat
						leagueDao.saveTeamMatch(objMatch);
					}
					else
					{
						//Si ja s'ha fet la final, tancar la competici�
						objLeague.setStatus("finalized");
						objLeague.setEnd(new Date());
						leagueDao.saveLeague(objLeague);
					}
				}
				
				//Desar canvis
				return leagueDao.saveTeamMatch(tempMatch);
			}
			
			//No ha calgut afegir la puntuaci� a l'arbre perqu� no tots els equips la
			//havien enviat
			return "OK";
		}
		
		return "VALIDATION_ERROR";
	}
	
	public LeagueDao getleagueDao()
	{
		return leagueDao;
	}
	
	public void setleagueDao(LeagueDao _lDao)
	{
		leagueDao = _lDao;
	}
	
	public SessionBO getsBo() {
		return sBo;
	}

	public void setsBo(SessionBO sBo) {
		this.sBo = sBo;
	}
	
	public GameBO getgBo() {
		return gBo;
	}

	public void setgBo(GameBO gBo) {
		this.gBo = gBo;
	}
	
	public GameInstanceBO getiBo() {
		return iBo;
	}

	public void setiBo(GameInstanceBO iBo) {
		this.iBo = iBo;
	}
}
