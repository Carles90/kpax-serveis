package uoc.edu.svrKpax.bussines;

import java.util.List;

import uoc.edu.svrKpax.vo.Achievement;
import uoc.edu.svrKpax.vo.AchievementRequirement;
import uoc.edu.svrKpax.vo.UserAchievement;

public interface AchievementBO {
	public List<Achievement> getGameAchievements(int gameId);
	public List<UserAchievement> getUserAchievementsForGame(int gameId, String username);
	public String addAchievementToGame(String campusSession, int idGame, String achName, String achDesc, int maxLevel);
	public String editAchievement(String campusSession, int achId, String achName, String achDesc, int maxLevel);
	public List<AchievementRequirement> getAchievementRequirements(int idAch);
	public String addRequirementToAchievement(String campusSession, int idAch, int requires);
	public Achievement getAchievement(int idAch);
	public List<AchievementRequirement> getAllAchievementRequirements();
	public String deleteAchievementFromGame(String campusSession, int idAch);
	public String deleteAllAchievementRequirements(String campusSession, int idAch);
	public String unlockAchievement(String campusSession, String uidGame, int idAch, int level);
	public List<UserAchievement> getAllUserAchievement();
}