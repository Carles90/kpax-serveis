package uoc.edu.svrKpax.bussines;

import java.util.List;

import uoc.edu.svrKpax.bussines.AchievementBO;
import uoc.edu.svrKpax.dao.AchievementDao;
import uoc.edu.svrKpax.dao.UserDao;
import uoc.edu.svrKpax.vo.Achievement;
import uoc.edu.svrKpax.vo.AchievementRequirement;
import uoc.edu.svrKpax.vo.Game;
import uoc.edu.svrKpax.vo.UserAchievement;
import uoc.edu.svrKpax.vo.User;

public class AchievementBOImp implements AchievementBO {
	
	private SessionBO sBo;
	private GameBO gBo;
	private AchievementDao achDao;
	private UserDao uDao;
	private GameInstanceBO iBo;
	
	public List<Achievement> getGameAchievements(int gameId) {
		List<Achievement> achList = achDao.getGameAchievements(gameId);
		return achList;
	}
	
	public List<UserAchievement> getUserAchievementsForGame(int gameId, String username){
		User objUser = uDao.getUserForUserName(username);
		List<UserAchievement> achList = achDao.getUserAchievementsForGame(gameId, objUser.getIdUser());
		return achList;
	}
	
	public String addAchievementToGame(String campusSession, int idGame, String achName, String achDesc, int maxLevel){
		if (sBo.validateSession(campusSession) != null) {
			Achievement achObj = new Achievement();
			
			achObj.setName(achName);
			achObj.setDescription(achDesc);
			achObj.setMaxLevel(maxLevel);
			achObj.setIdGame(idGame);
			
			return achDao.saveAchievement(achObj);
		}
		return "INVALID_SESSION";
	}
	
	public String editAchievement(String campusSession, int achId, String achName, String achDesc, int maxLevel){
		if (sBo.validateSession(campusSession) != null) {
			Achievement achObj = getAchievement(achId);
			
			if(achObj == null)
			{
				return "NULL_ACHIEVEMENT";
			}
			
			achObj.setName(achName);
			achObj.setDescription(achDesc);
			achObj.setMaxLevel(maxLevel);
			
			return achDao.saveAchievement(achObj);
		}
		return "INVALID_SESSION";
	}
	
	public List<AchievementRequirement> getAchievementRequirements(int idAch) {
		return achDao.getAchievementRequirements(idAch);
	}
	
	public String addRequirementToAchievement(String campusSession, int idAch, int requires){
		//Validar la sessi� de l'usuari
		if (sBo.validateSession(campusSession) != null) {
			
			Achievement achObj = getAchievement(idAch);
			Achievement reqObj = getAchievement(requires);
			
			//Comprovar si els assoliments existeixen mitjan�ant la seva ID
			if(achObj == null || reqObj == null){
				return "NULL_ACHIEVEMENTS";
			}
			
			//Comprovar que ambd�s assoliments pertanyin al mateix joc
			if(achObj.getIdGame() != reqObj.getIdGame()){
				return "DIFFERENT_GAMES";
			}
			
			//Comprovar que un assoliment no es requereixi a ell mateix
			if(achObj.getIdAchievement() == reqObj.getIdAchievement()){
				return "RECURSIVE_REQUIREMENT";
			}
			
			AchievementRequirement achreq = new AchievementRequirement();
			
			achreq.setIdAchievement(idAch);
			achreq.setRequires(requires);
			
			return achDao.addRequirementToAchievement(achreq);
		}
		
		//Si la sessi� �s inv�lida...
		return "INVALID_SESSION";
	}
	
	public String deleteAchievementFromGame(String campusSession, int idAch)
	{
		if (sBo.validateSession(campusSession) != null) {
			Achievement achObj = getAchievement(idAch);
			
			if(achObj == null){
				return "NULL_ACHIEVEMENT";
			}
			
			return achDao.deleteAchievementFromGame(achObj);
		}
		
		return "INVALID_SESSION";
	}
	
	public List<AchievementRequirement> getAllAchievementRequirements()
	{
		return achDao.getAllAchievementRequirements();
	}
	
	public String deleteAllAchievementRequirements(String campusSession, int idAch)
	{
		if (sBo.validateSession(campusSession) != null) {
			List<AchievementRequirement> achReqs = getAchievementRequirements(idAch);
			for(AchievementRequirement achReq : achReqs)
			{
				achDao.deleteRequirement(achReq);
			}
			
			return "OK";
		}
		
		return "INVALID_SESSION";
	}
	
	public String unlockAchievement(String campusSession, String uidGame, int idAch, int level)
	{
		//Validar la sessi� de l'usuari
		User objUser = sBo.validateSession(campusSession);
		
		if (objUser != null) 
		{
			//Obtenci� del joc al que pertany l'assoliment indicat
			Game objGame = gBo.getGame(uidGame, null);
			Achievement objAch = getAchievement(idAch);
			
			//Si el joc no existeix, acabar
			if(objGame == null)
			{
				return "INVALID_GAME";
			}
			
			//Comprovar que una inst�ncia del joc hagi estat creada pr�viament
			if(!iBo.isInstanceCreated(campusSession, uidGame))
			{
				return "INSTANCE_NOT_CREATED";
			}
			
			//Si l'assoliment no existeix, acabar
			if(objAch == null)
			{
				return "INVALID_ACHIEVEMENT";
			}
			
			//Si l'assoliment no pertany al joc, acabar
			if(objGame.getIdGame() != objAch.getIdGame())
			{
				return "WRONG_GAME";
			}
			
			//Comprovar si l'usuari ja poseeix l'assoliment
			List<UserAchievement> userAch = getUserAchievementsForGame(objGame.getIdGame(), objUser.getLogin());
			
			for(UserAchievement uAch : userAch)
			{
				if(uAch.getIdAchievement() == objAch.getIdAchievement())
				{
					if(objAch.getMaxLevel() > 0)
					{
						//Si l'assoliment funciona per puntuaci�, comprovar que li estiguem donant una puntuaci� superior
						if(level <= uAch.getLevel())
						{
							return "LEVEL_IS_LOWER";
						}
						else if(level > objAch.getMaxLevel())
						{
							return "LEVEL_IS_HIGHER_THAN_MAX";
						}
						else
						{
							uAch.setLevel(level);
							return achDao.saveUserAchievement(uAch);
						}
					}
					else
					{
						return "ALREADY_HAVING";
					}
				}
			}
			
			//Comprovar que es compleixin tots els requeriments
			List<AchievementRequirement> achReqs = getAchievementRequirements(idAch);
			for(AchievementRequirement req : achReqs)
			{
				boolean found = false;
				for(UserAchievement uAch : userAch)
				{
					if(uAch.getIdAchievement() == req.getRequires())
					{
						found = true;
					}
				}
				
				if(!found)
				{
					return "NOT_ALL_REQUIREMENTS";
				}
			}
			
			//Creaci� d'un nou assoliment d'usuari
			UserAchievement uAchObj = new UserAchievement();
				
			uAchObj.setIdAchievement(idAch);
			uAchObj.setIdUser(objUser.getIdUser());
			
			if(objAch.getMaxLevel() > 0 && level <= objAch.getMaxLevel())
			{
				uAchObj.setLevel(level);
			}
			else
			{
				uAchObj.setLevel(0);
			}
			
			return achDao.saveUserAchievement(uAchObj);
		}
		return "INVALID_SESSION";
	}
	
	public List<UserAchievement> getAllUserAchievement()
	{
		return achDao.getAllUserAchievement();
	}
	
	public Achievement getAchievement(int idAch){
		return achDao.getAchievement(idAch);
	}
	
	public AchievementDao getachDao() {
		return achDao;
	}

	public void setachDao(AchievementDao achDao) {
		this.achDao = achDao;
	}
	
	public void setuDao(UserDao uDao) {
		this.uDao = uDao;
	}

	public UserDao getuDao() {
		return uDao;
	}
	
	public SessionBO getsBo() {
		return sBo;
	}

	public void setsBo(SessionBO sBo) {
		this.sBo = sBo;
	}
	
	public GameBO getgBo() {
		return gBo;
	}

	public void setgBo(GameBO gBo) {
		this.gBo = gBo;
	}
	
	public GameInstanceBO getiBo() {
		return iBo;
	}

	public void setiBo(GameInstanceBO iBo) {
		this.iBo = iBo;
	}

	
}