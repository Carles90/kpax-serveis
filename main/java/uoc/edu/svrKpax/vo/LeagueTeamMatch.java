package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "leagueteammatch")
public class LeagueTeamMatch implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int idTeamMatch;
	private int idLeague;
	private Integer idTeam1; //Ha de ser Integer per a que es pugui instanciar a NULL
	private Integer idTeam2;
	private int treeHeight;
	private int treePos;
	private Integer winner;
	private int scoreTeam1;
	private int scoreTeam2;
	private int status;
	
	@Id
	@Column(name = "idTeamMatch")
	public int getIdTeamMatch() {
		return idTeamMatch;
	}
	public void setIdTeamMatch(int idTeamMatch) {
		this.idTeamMatch = idTeamMatch;
	}
	
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
	
	@Column(name = "idTeam1")
	public Integer getIdTeam1() {
		return idTeam1;
	}
	public void setIdTeam1(Integer idTeam1) {
		this.idTeam1 = idTeam1;
	}
	
	@Column(name = "idTeam2")
	public Integer getIdTeam2() {
		return idTeam2;
	}
	public void setIdTeam2(Integer idTeam2) {
		this.idTeam2 = idTeam2;
	}
	
	@Column(name = "treeHeight")
	public int getTreeHeight() {
		return treeHeight;
	}
	public void setTreeHeight(int treeHeight) {
		this.treeHeight = treeHeight;
	}
	
	@Column(name = "treePos")
	public int getTreePos() {
		return treePos;
	}
	public void setTreePos(int treePos) {
		this.treePos = treePos;
	}
	
	@Column(name = "winner")
	public Integer getWinner() {
		return winner;
	}
	public void setWinner(Integer winner) {
		this.winner = winner;
	}
	
	@Column(name = "scoreTeam1")
	public int getScoreTeam1() {
		return scoreTeam1;
	}
	public void setScoreTeam1(int scoreTeam1) {
		this.scoreTeam1 = scoreTeam1;
	}
	
	@Column(name = "scoreTeam2")
	public int getScoreTeam2() {
		return scoreTeam2;
	}
	public void setScoreTeam2(int scoreTeam2) {
		this.scoreTeam2 = scoreTeam2;
	}
	
	@Column(name = "status")
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}