package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "leagueusersinfo")
public class LeagueUserInfo implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	private int idLeagueUser;
	private int idUser;
	private String username;
	private Integer idTeam;
	private int idLeague;
	
	@Id
	@Column(name = "idLeagueUser")
	public int getIdLeagueUser() {
		return idLeagueUser;
	}
	public void setIdLeagueUser(int idLeagueUser) {
		this.idLeagueUser = idLeagueUser;
	}
	
	@Column(name = "idUser")
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	@Column(name = "username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name = "idTeam")
	public Integer getIdTeam() {
		return idTeam;
	}
	public void setIdTeam(Integer idTeam) {
		this.idTeam = idTeam;
	}
	
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
	
	
}
