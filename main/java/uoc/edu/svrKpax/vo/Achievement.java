package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "achievement")
public class Achievement implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idAchievement;
	private int idGame;
	private String name;
	private String description;
	private int maxLevel;
	
	@Id
	@Column(name = "idAchievement")
	public int getIdAchievement() {
		return idAchievement;
	}
	
	public void setIdAchievement(int id){
		idAchievement = id;
	}
	
	@Column(name = "name")
	public String getName(){
		return name;
	}
	
	public void setName(String _name){
		name = _name;
	}
	
	@Column(name = "description")
	public String getDescription(){
		return description;
	}
	
	public void setDescription(String _description){
		description = _description;
	}
	
	@Column(name = "maxLevel")
	public int getMaxLevel() {
		return maxLevel;
	}
	
	public void setMaxLevel(int _level){
		maxLevel = _level;
	}
	
	@Column(name = "idGame")
	public int getIdGame() {
		return idGame;
	}
	
	public void setIdGame(int _idGame){
		idGame = _idGame;
	}
}