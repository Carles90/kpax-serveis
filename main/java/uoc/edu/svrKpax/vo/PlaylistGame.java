package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "playlistgames")
public class PlaylistGame implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idPlayListGame")
	private int idPlaylistGame;
	private int idGame;
	private int idLeague;
	private int gameOrder;
	private String automaticallyAdded;
	
	public int getIdPlaylistGame() {
		return idPlaylistGame;
	}
	public void setIdPlaylistGame(int idPlaylistGame) {
		this.idPlaylistGame = idPlaylistGame;
	}
	
	@Column(name = "idGame")
	public int getIdGame() {
		return idGame;
	}
	public void setIdGame(int idGame) {
		this.idGame = idGame;
	}
	
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
	
	@Column(name = "gameOrder")
	public int getGameOrder() {
		return gameOrder;
	}
	public void setGameOrder(int order) {
		this.gameOrder = order;
	}
	
	@Column(name = "automaticallyAdded")
	public String getAutomaticallyAdded() {
		return automaticallyAdded;
	}
	public void setAutomaticallyAdded(String automaticallyAdded) {
		this.automaticallyAdded = automaticallyAdded;
	}
}
