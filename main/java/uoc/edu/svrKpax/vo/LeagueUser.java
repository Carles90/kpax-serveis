package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "leagueusers")
public class LeagueUser implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int idLeagueUser;
	private int idLeague;
	private int idUser;
	private Integer idTeam;
	
	@Id
	@Column(name = "idLeagueUser")
	public int getIdLeagueUser() {
		return idLeagueUser;
	}
	public void setIdLeagueUser(int idLeagueUser) {
		this.idLeagueUser = idLeagueUser;
	}
	
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
	
	@Column(name = "idUser")
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	@Column(name = "idTeam")
	public Integer getIdTeam() {
		return idTeam;
	}
	public void setIdTeam(Integer idTeam) {
		this.idTeam = idTeam;
	}
}
