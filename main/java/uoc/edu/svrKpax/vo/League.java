package uoc.edu.svrKpax.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "league")
public class League implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int idLeague;
	private int idOwner;
	@Temporal(TemporalType.TIMESTAMP)
	private Date start;
	@Temporal(TemporalType.TIMESTAMP)
	private Date end;
	private String status;
	private String scoreType;
	private String distribution;
	private int maxUsers;
	private int maxGroups;
	private int maxUsersGroup;
	private String title;
	private String description;
	private String news;
	private String allowTeamCreation;
	
	@Id
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
	
	@Column(name = "idOwner")
	public int getIdOwner() {
		return idOwner;
	}
	public void setIdOwner(int idOwner) {
		this.idOwner = idOwner;
	}
	
	@Column(name = "start")
	public Date getStart()
	{
		return start;
	}
	public void setStart(Date start)
	{
		this.start = start;
	}
	
	@Column(name = "end")
	public Date getEnd()
	{
		return end;
	}
	public void setEnd(Date end)
	{
		this.end = end;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "scoreType")
	public String getScoreType() {
		return scoreType;
	}
	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}
	
	@Column(name = "distribution")
	public String getDistribution() {
		return distribution;
	}
	public void setDistribution(String distribution) {
		this.distribution = distribution;
	}
	
	@Column(name = "maxUsers")
	public int getMaxUsers() {
		return maxUsers;
	}
	public void setMaxUsers(int maxUsers) {
		this.maxUsers = maxUsers;
	}
	
	@Column(name = "maxGroups")
	public int getMaxGroups() {
		return maxGroups;
	}
	public void setMaxGroups(int maxGroups) {
		this.maxGroups = maxGroups;
	}
	
	@Column(name = "maxUsersGroup")
	public int getMaxUsersGroup() {
		return maxUsersGroup;
	}
	public void setMaxUsersGroup(int maxUsersGroup) {
		this.maxUsersGroup = maxUsersGroup;
	}
	
	@Column(name = "title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name = "news")
	public String getNews() {
		return news;
	}
	public void setNews(String news) {
		this.news = news;
	}
	
	@Column(name = "allowTeamCreation")
	public String getAllowTeamCreation() {
		return allowTeamCreation;
	}
	public void setAllowTeamCreation(String allowTeamCreation) {
		this.allowTeamCreation = allowTeamCreation;
	}
}
