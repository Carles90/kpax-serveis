package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "leagueteamsplaylistscore")
public class LeagueTeamPlaylistScore implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idScore")
	private int idScore;
	private int idPlaylistGame;
	private int avgscore;
	private int submits;
	private int idTeam;
	private int idLeague;
	
	@Column(name = "idPlaylistGame")
	public int getIdPlaylistGame() {
		return idPlaylistGame;
	}
	public void setIdPlaylistGame(int idPlaylistGame) {
		this.idPlaylistGame = idPlaylistGame;
	}
	
	@Column(name = "avgscore")
	public int getAvgscore() {
		return avgscore;
	}
	public void setAvgscore(int avgscore) {
		this.avgscore = avgscore;
	}
	
	@Column(name = "submits")
	public int getSubmits() {
		return submits;
	}
	public void setSubmits(int submits) {
		this.submits = submits;
	}
	
	@Column(name = "idTeam")
	public int getIdTeam() {
		return idTeam;
	}
	public void setIdTeam(int idTeam) {
		this.idTeam = idTeam;
	}
	
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
}