package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "leagueusermatch")
public class LeagueUserMatch implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int idUserMatch;
	private int idLeague;
	private Integer idUser1; //Ha de ser Integer per a que es pugui instanciar a NULL
	private Integer idUser2;
	private int treeHeight;
	private int treePos;
	private Integer winner;
	private int scoreUser1;
	private int scoreUser2;
	private int status;
	
	@Id
	@Column(name = "idUserMatch")
	public int getIdUserMatch() {
		return idUserMatch;
	}
	public void setIdUserMatch(int idUserMatch) {
		this.idUserMatch = idUserMatch;
	}
	
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
	
	@Column(name = "idUser1")
	public Integer getIdUser1() {
		return idUser1;
	}
	public void setIdUser1(Integer idUser1) {
		this.idUser1 = idUser1;
	}
	
	@Column(name = "idUser2")
	public Integer getIdUser2() {
		return idUser2;
	}
	public void setIdUser2(Integer idUser2) {
		this.idUser2 = idUser2;
	}
	
	@Column(name = "treeHeight")
	public int getTreeHeight() {
		return treeHeight;
	}
	public void setTreeHeight(int treeHeight) {
		this.treeHeight = treeHeight;
	}
	
	@Column(name = "treePos")
	public int getTreePos() {
		return treePos;
	}
	public void setTreePos(int treePos) {
		this.treePos = treePos;
	}
	
	@Column(name = "winner")
	public Integer getWinner() {
		return winner;
	}
	public void setWinner(Integer winner) {
		this.winner = winner;
	}
	
	@Column(name = "scoreUser1")
	public int getScoreUser1() {
		return scoreUser1;
	}
	public void setScoreUser1(int scoreUser1) {
		this.scoreUser1 = scoreUser1;
	}
	
	@Column(name = "scoreUser2")
	public int getScoreUser2() {
		return scoreUser2;
	}
	public void setScoreUser2(int scoreUser2) {
		this.scoreUser2 = scoreUser2;
	}
	
	@Column(name = "status")
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
