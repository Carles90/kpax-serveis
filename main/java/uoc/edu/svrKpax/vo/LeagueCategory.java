package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "leaguecategory")
public class LeagueCategory implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int idLeagueCategory;
	private int idLeague;
	private int idCategory;
	
	@Id
	@Column(name = "idLeagueCategory")
	public int getIdLeagueCategory() {
		return idLeagueCategory;
	}
	public void setIdLeagueCategory(int idLeagueCategory) {
		this.idLeagueCategory = idLeagueCategory;
	}
	
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
	
	@Column(name = "idCategory")
	public int getIdCategory() {
		return idCategory;
	}
	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}
	
}
