package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "UserAchievement")
public class UserAchievement implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int idUserAchievement;
	private int idAchievement;
	private int idUser;
	private int level;
	
	@Id
	@Column(name = "idUserAchievement")
	public int getIdUserAchievement()
	{
		return idUserAchievement;
	}
	
	public void setIdUserAchievement(int id)
	{
		idUserAchievement = id;
	}
	
	@Column(name = "idAchievement")
	public int getIdAchievement(){
		return idAchievement;
	}
	
	public void setIdAchievement(int _id){
		idAchievement = _id;
	}
	
	@Column(name = "idUser")
	public int getIdUser(){
		return idUser;
	}
	
	public void setIdUser(int _id){
		idUser = _id;
	}
	
	@Column(name = "level")
	public int getLevel(){
		return level;
	}
	
	public void setLevel(int _level){
		level = _level;
	}
}