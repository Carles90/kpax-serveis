package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

@XmlRootElement
@Entity
@Table(name = "leagueusersscore")
public class LeagueUserScore implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GenericGenerator(name="gen",strategy="increment")
	@GeneratedValue(generator="gen")
	@Column(name = "idScore")
	private int idScore;
	private int idPlaylistGame;
	private int idLeagueUser;
	private int score;
	
	public int getIdScore() {
		return idScore;
	}
	public void setIdScore(int idScore) {
		this.idScore = idScore;
	}
	
	@Column(name = "idPlaylistGame")
	public int getIdPlaylistGame() {
		return idPlaylistGame;
	}
	public void setIdPlaylistGame(int idPlaylistGame) {
		this.idPlaylistGame = idPlaylistGame;
	}
	
	@Column(name = "idLeagueUser")
	public int getIdLeagueUser() {
		return idLeagueUser;
	}
	public void setIdLeagueUser(int idLeagueUser) {
		this.idLeagueUser = idLeagueUser;
	}
	
	@Column(name = "score")
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
}
