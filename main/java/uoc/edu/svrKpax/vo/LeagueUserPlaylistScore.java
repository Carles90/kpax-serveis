package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

@XmlRootElement
@Entity
@Table(name = "leagueusersplaylistscore")
public class LeagueUserPlaylistScore implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GenericGenerator(name="gen",strategy="increment")
	@GeneratedValue(generator="gen")
	@Column(name = "idScore")
	private int idScore;
	private int idPlaylistGame;
	private int idLeagueUser;
	private int score;
	private int idUser;
	private Integer idTeam; //Pot ser nul
	private int idLeague;
	
	public int getIdScore() {
		return idScore;
	}
	public void setIdScore(int idScore) {
		this.idScore = idScore;
	}
	
	@Column(name = "idPlaylistGame")
	public int getIdPlaylistGame() {
		return idPlaylistGame;
	}
	public void setIdPlaylistGame(int idPlaylistGame) {
		this.idPlaylistGame = idPlaylistGame;
	}
	
	@Column(name = "idLeagueUser")
	public int getIdLeagueUser() {
		return idLeagueUser;
	}
	public void setIdLeagueUser(int idLeagueUser) {
		this.idLeagueUser = idLeagueUser;
	}
	
	@Column(name = "score")
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
	@Column(name = "idUser")
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	@Column(name = "idTeam")
	public Integer getIdTeam() {
		return idTeam;
	}
	public void setIdTeam(Integer idTeam) {
		this.idTeam = idTeam;
	}
	
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
}
