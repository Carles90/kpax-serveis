package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

@XmlRootElement
@Entity
@Table(name = "gameuserbelonging")
public class GameUserBelonging implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GenericGenerator(name="gen", strategy="increment")
	@GeneratedValue(generator="gen")
	@Column(name = "idBelonging")
	private int idBelonging;
	private int idUser;
	private int idGame;
	
	public int getIdBelonging()
	{
		return idBelonging;
	}
	
	public void setIdBelonging(int _id)
	{
		idBelonging = _id;
	}
	
	@Column(name = "idUser")
	public int getIdUser()
	{
		return idUser;
	}
	
	public void setIdUser(int _id)
	{
		idUser = _id;
	}
	
	@Column(name = "idGame")
	public int getIdGame()
	{
		return idGame;
	}
	
	public void setIdGame(int _id)
	{
		idGame = _id;
	}
}
