package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

@XmlRootElement
@Entity
@Table(name = "leagueteamsmembers")
public class LeagueTeamMembers implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GenericGenerator(name="gen",strategy="increment")
	@GeneratedValue(generator="gen")
	@Column(name = "idTeam")
	private int idTeam;
	
	private int idLeague;
	private Integer idOwner;
	private String teamName;
	private String teamPassword;
	private int members;
	
	public int getIdTeam() {
		return idTeam;
	}
	public void setIdTeam(int idTeam) {
		this.idTeam = idTeam;
	}
	
	@Column(name = "idLeague")
	public int getIdLeague() {
		return idLeague;
	}
	public void setIdLeague(int idLeague) {
		this.idLeague = idLeague;
	}
	
	@Column(name = "idOwner")
	public Integer getIdOwner() {
		return idOwner;
	}
	public void setIdOwner(Integer idOwner) {
		this.idOwner = idOwner;
	}
	
	@Column(name = "teamName")
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	
	@Column(name = "teamPassword")
	public String getTeamPassword() {
		return teamPassword;
	}
	public void setTeamPassword(String teamPassword) {
		this.teamPassword = teamPassword;
	}
	
	@Column(name = "members")
	public int getMembers() {
		return members;
	}
	public void setMembers(int members) {
		this.members = members;
	}
}
