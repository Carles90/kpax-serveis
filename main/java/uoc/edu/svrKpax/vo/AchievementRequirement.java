package uoc.edu.svrKpax.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "achievementreqs")
public class AchievementRequirement implements Serializable{
	private static final long serialVersionUID = 1L;
	private int idRequirement;
	private int idAchievement;
	private int requires;
	
	@Id
	@Column(name = "idRequirement")
	public int getIdRequirement(){
		return idRequirement;
	}
	
	public void setIdRequirement(int id){
		idRequirement = id;
	}
	
	@Column(name = "idAchievement")
	public int getIdAchievement(){
		return idAchievement;
	}
	
	public void setIdAchievement(int _id){
		idAchievement = _id;
	}
	
	@Column(name = "requires")
	public int getRequires(){
		return requires;
	}
	
	public void setRequires(int _id){
		requires = _id;
	}
}