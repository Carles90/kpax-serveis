package uoc.edu.svrKpax.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import uoc.edu.svrKpax.bussines.AchievementBO;
import uoc.edu.svrKpax.bussines.CategoryBO;
import uoc.edu.svrKpax.bussines.CommentBO;
import uoc.edu.svrKpax.bussines.GameBO;
import uoc.edu.svrKpax.bussines.GameInstanceBO;
import uoc.edu.svrKpax.bussines.GameLikeBO;
import uoc.edu.svrKpax.bussines.GameScoreBO;
import uoc.edu.svrKpax.bussines.LeagueBO;
import uoc.edu.svrKpax.bussines.TagBO;
import uoc.edu.svrKpax.util.AES;
import uoc.edu.svrKpax.vo.AchievementRequirement;
import uoc.edu.svrKpax.vo.Category;
import uoc.edu.svrKpax.vo.Comment;
import uoc.edu.svrKpax.vo.Game;
import uoc.edu.svrKpax.vo.GameLike;
import uoc.edu.svrKpax.vo.GameUserBelonging;
import uoc.edu.svrKpax.vo.League;
import uoc.edu.svrKpax.vo.LeagueTeam;
import uoc.edu.svrKpax.vo.LeagueTeamMatch;
import uoc.edu.svrKpax.vo.LeagueTeamMembers;
import uoc.edu.svrKpax.vo.LeagueTeamPlaylistScore;
import uoc.edu.svrKpax.vo.LeagueUser;
import uoc.edu.svrKpax.vo.LeagueUserInfo;
import uoc.edu.svrKpax.vo.LeagueUserMatch;
import uoc.edu.svrKpax.vo.LeagueUserPlaylistScore;
import uoc.edu.svrKpax.vo.Score;
import uoc.edu.svrKpax.vo.Tag;
import uoc.edu.svrKpax.vo.Achievement;
import uoc.edu.svrKpax.vo.UserAchievement;

import com.sun.jersey.spi.inject.Inject;

@SuppressWarnings("deprecation")
@Path("/game")
public class Games {

	@Inject
	private GameBO gBo;
	@Inject
	private GameLikeBO lBo;
	@Inject
	private GameInstanceBO iBo;
	@Inject
	private GameScoreBO scBo;
	@Inject
	private CategoryBO catBo;
	@Inject
	private TagBO tagBo;
	@Inject
	private CommentBO comBo;
	@Inject
	private AchievementBO achBo;
	@Inject
	private LeagueBO leagueBo;
	

	/* GAMES */
	/*@GET
	@Path("/{param}/list")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<Game> getGames(@PathParam("param") String campusSession) {
		return gBo.listGames(campusSession);
	}
	
	
	@POST
	@Path("/add")
	public String addGame(@FormParam("secretSession") String campusSession,@FormParam("name") String nameGame,@FormParam("idGame") String idGame){
		if(gBo.addGame(campusSession, nameGame,Integer.parseInt(idGame))){
			return "OK";
		}else{
			return "ERROR";
		}
	}*/
	
	@POST
	@Path("/{param}/list/{idOrderer}/{idFilterer}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<Game> getGames(@PathParam("param") String campusSession, @PathParam("idOrderer") int idOrderer, @PathParam("idFilterer") int idFilterer, @FormParam("fields") List<String> fields, @FormParam("values")  List<String> values) {
		return gBo.listGames(campusSession, idOrderer, idFilterer, fields, values);
	}
	
	@POST
	@Path("/game/{session}/listDev/{username}") 
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	//
	// No sobra el primer /game?????????????????????
	//
	public List<Game> getUserGames(@PathParam("username") String username,@PathParam("session") String campusSession) {
		return gBo.listUserGames(username, campusSession);
	}

	@POST
	@Path("/add")
	public String addGame(@FormParam("secretSession") String campusSession,@FormParam("name") String nameGame,@FormParam("idGame") String idGame, @FormParam("idCategory") String idCategory, @FormParam("creationDate") String creationDate ){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		sdf.setLenient(false);
		try
		{
			if(gBo.addGame(campusSession, nameGame,Integer.parseInt(idGame), Integer.parseInt(idCategory), sdf.parse(creationDate))){
				return "OK";
			}
		}
		catch (NumberFormatException e) {}
		catch (ParseException e) {}
		return "ERROR";
	}
	
	@GET
	@Path("/{param}/get/{id}")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public Game getGame(@PathParam("id") String idGame,@PathParam("param") String secretSession){
		return gBo.getGame(idGame,secretSession);
	}
	
	@POST
	@Path("/delete/{id}")
	public String delGame(@FormParam("secretSession") String campusSession,@PathParam("id") String idGame){
		if(gBo.delGame(campusSession, Integer.parseInt(idGame)))return "OK";
		else
			return "ERROR";
	}
	
	/* categories */
	@GET
	@Path("/category/{param}/list")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<Category> getCategories (@PathParam("param") String campusSession){
		return catBo.listCategories(campusSession);
	}
	
	@GET
	@Path("/category/{param}/get/{id}")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public Category getCategory (@PathParam("param") String campusSession, @PathParam("id") int idCategory){
		return catBo.getCategory(campusSession, idCategory);
	}
	
	/* tags */
	@GET
	@Path("/tag/{param}/list/{id}")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<Tag> getTagsGame (@PathParam("param") String campusSession, @PathParam("id") int idGame){
		return tagBo.listTagsGame(campusSession, idGame);
	}
	
	@POST
	@Path("/tag/{id}/addDel")
	public String addDelTagsGame(@FormParam("secretSession") String campusSession,@PathParam("id") int idGame,@FormParam("tags") String tagsCommaSeparated){
		String [] tagsSplit = tagsCommaSeparated.split(",");
		List<Tag> tags = new ArrayList<Tag>();
		for(String tagValue : tagsSplit)
		{
			tagValue = tagValue.trim();
			if(tagValue.equals(""))
				continue;
			Tag tag = new Tag();
			tag.setTag(tagValue);
			tags.add(tag);
		}
		if(tagBo.addTagsGame(campusSession, idGame, tags))
			return "OK";
		else
			return "ERROR";
	}
	
	/* comments */
	@GET
	@Path("/comment/{param}/list/{id}")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<Comment> getCommentsGame (@PathParam("param") String campusSession, @PathParam("id") int idGame){
		return comBo.listCommentsGame(campusSession, idGame);
	}
	
	@POST
	@Path("/comment/{id}/add")
	public String addCommentGame(@FormParam("secretSession") String campusSession,@PathParam("id") int idComment, @FormParam("idGame") int idGame){
		if(comBo.addComment(campusSession, idComment, idGame))
			return "OK";
		else
			return "ERROR";
	}
	
	@POST
	@Path("/comment/{id}/del")
	public String delCommentGame(@FormParam("secretSession") String campusSession,@PathParam("id") int idComment){
		if(comBo.delComment(campusSession, idComment))
			return "OK";
		else
			return "ERROR";
	}

	/* likes */
	@GET
	@Path("/like/{param}/get/{id}")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public GameLike getLikeGame (@PathParam("param") String campusSession,@PathParam("id") int idGame){
		return lBo.getLikeGame(campusSession, idGame);
	}
	
	@POST
	@Path("/like/{id}/add")
	public String likeAddGame(@FormParam("secretSession") String campusSession,@PathParam("id") String idGame,@FormParam("containerId") String containerId){
		if(lBo.addLikeGame(campusSession, Integer.parseInt(idGame),containerId))return "OK";
		else
			return "ERROR";
	}
	
	@POST
	@Path("/like/{id}/del")
	public String likeDelGame(@FormParam("secretSession") String campusSession,@PathParam("id") String idGame,@FormParam("containerId") String containerId){
		if(lBo.delLikeGame(campusSession, Integer.parseInt(idGame)))return "OK";
		else
			return "ERROR";
	}
	@POST
	@Path("/like/{id}")
	public String likeGame(@FormParam("secretSession") String campusSession,@PathParam("id") String idGame,@FormParam("containerId") String containerId){
		if(lBo.addDelLikeGame(campusSession, Integer.parseInt(idGame),containerId))return "OK";
		else
			return "ERROR";
	}

	@GET
	@Path("/like/{param}/list/{id}")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<GameLike> getLikesGame(@PathParam("param") String campusSession,@PathParam("id") int idGame){	
		return lBo.listLikeGames(campusSession,idGame);
	}
	
	/* GAME INSTANCES */
	
	@POST
	@Path("/instance/init")
	public Response initGame(@FormParam("secretSession") String campusSession,@FormParam("secretGame") String uidGame){
		return iBo.initGameInstance(campusSession, uidGame);
	}
	
	@POST
	@Path("/instance/end/score")
	public Response endGameScore(@FormParam("secretSession") String campusSession,@FormParam("secretGame") String uidGame,@FormParam("points") String points) throws Exception{
		if(iBo.entGameInstance(campusSession, uidGame).getStatus() == 200){
			return scBo.addScoreGame(campusSession, uidGame,AES.descrypt(points));
		}else
			return Response.status(404).entity("Error end instance").build();
	}
	
	@POST
	@Path("/instance/end")
	public Response endGameInstance(@FormParam("secretSession") String campusSession,@FormParam("secretGame") String uidGame){
		return iBo.entGameInstance(campusSession, uidGame);
	}
	
	/* GAME SCORE */
	
	@GET
	@Path("/score/{param}/list")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<Score> getScoresGame(@PathParam("param") String uidGame){
		return scBo.listScoreGames(uidGame);
	}
	
	/* Game Playing Users */
	
	@GET
	@Path("/{idgame}/usersplaying/list")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<GameUserBelonging> getPlayingUsers(@PathParam("idgame") int idGame)
	{
		return gBo.getPlayingUsers(idGame);
	}

	/* Game Achievements */
	@GET
	@Path("/achievement/game/{id}/list")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<Achievement> getGameAchievements(@PathParam("id") int uidGame){
		return achBo.getGameAchievements(uidGame);
	}
	
	@GET
	@Path("/achievement/single/{id}/get")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public Achievement getAchievement(@PathParam("id") int idAch){
		return achBo.getAchievement(idAch);
	}
	
	@GET
	@Path("/userachievements/{idgame}/{username}/list")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<UserAchievement> getUserAchievementsForGame(@PathParam("idgame") int idGame, @PathParam("username") String username){
		return achBo.getUserAchievementsForGame(idGame, username);
	}
	
	@POST
	@Path("/achievement/add/{idgame}")
	public String addAchievementToGame(@PathParam("idgame") int idGame, @FormParam("campusSession") String campusSession, @FormParam("achName") String achName, @FormParam("achDesc") String achDesc, @FormParam("achMaxLevel") int maxLevel){
		System.out.println("ADDING ACHIEVEMENT: " + maxLevel);
		return achBo.addAchievementToGame(campusSession, idGame, achName, achDesc, maxLevel);
	}
	
	@POST
	@Path("/achievement/edit/{idach}")
	public String editAchievement(@PathParam("idach") int idAch, @FormParam("campusSession") String campusSession, @FormParam("achName") String achName, @FormParam("achDesc") String achDesc, @FormParam("achMaxLevel") int maxLevel){
		return achBo.editAchievement(campusSession, idAch, achName, achDesc, maxLevel);
	}
	
	@POST
	@Path("/achievement/deletereqs/{idach}")
	public String deleteAllAchievementRequirements(@PathParam("idach") int idAch, @FormParam("campusSession") String campusSession){
		return achBo.deleteAllAchievementRequirements(campusSession, idAch);
	}
	
	@POST
	@Path("/achievement/delete/{idachievement}")
	public String deleteAchievementFromGame(@PathParam("idachievement") int idAchievement, @FormParam("campusSession") String campusSession)
	{
		return achBo.deleteAchievementFromGame(campusSession, idAchievement);
	}
	
	@GET
	@Path("/achievement/reqs/{idach}/list")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<AchievementRequirement> getAchievementRequeriments(@PathParam("idach") int idAchievement){
		return achBo.getAchievementRequirements(idAchievement);
	}
	
	@POST
	@Path("/achievement/addreq/{idach}")
	public String addRequirementToAchievement(@PathParam("idach") int idAch, @FormParam("campusSession") String campusSession, @FormParam("requires") int requires){
		return achBo.addRequirementToAchievement(campusSession, idAch, requires);
	}
	
	@GET
	@Path("/achievementreqs/list")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<AchievementRequirement> getAllAchievementRequirements(){
		return achBo.getAllAchievementRequirements();
	}
	
	@POST
	@Path("/achievement/unlock/{idach}")
	public String unlockAchievement(@PathParam("idach") int idAch, @FormParam("campusSession") String campusSession, @FormParam("secretGame") String uidGame, @FormParam("level") int level)
	{
		return achBo.unlockAchievement(campusSession, uidGame, idAch, level);
	}
	
	@GET
	@Path("/achievementusers/list")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<UserAchievement> getAllUserAchievement(){
		return achBo.getAllUserAchievement();
	}
	
	/* Game Leagues */
	@GET
	@Path("/leagues/list/{filter}")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<League> getLeagues(@PathParam("filter") String filter)
	{
		return leagueBo.getLeagues(filter);
	}
	
	@POST
	@Path("/leagues/user/list")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<LeagueUser> getUserLeagues(@FormParam("campusSession") String campusSession)
	{
		return leagueBo.getUserLeagues(campusSession);
	}
	
	@POST
	@Path("/league/{id}/categories")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<Category> getLeagueCategories(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.getLeagueCategories(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/playlist")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<Game> getLeaguePlaylist(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.getLeaguePlaylist(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/view")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public League getLeague(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.getLeague(campusSession, idLeague);
	}
	
	@POST
	@Path("/leagues/add")
	public String addLeague(@FormParam("campusSession") String campusSession, 
			@FormParam("idLeague") int idLeague, 
			@FormParam("title") String title, 
			@FormParam("description") String description, 
			@FormParam("start") long start, 
			@FormParam("end") long end, 
			@FormParam("scoretype") String scoretype, 
			@FormParam("distribution") String distribution, 
			@FormParam("maxusers") int maxusers, 
			@FormParam("maxgroups") int maxgroups, 
			@FormParam("maxusergroup") int maxusergroup,
			@FormParam("allowteams") String allowteams)
	{
		return leagueBo.addLeague(campusSession, idLeague, title, description, 
				start, end, scoretype, distribution, maxusers, maxgroups, 
				maxusergroup, allowteams);
	}
	
	@POST
	@Path("/league/{idLeague}/edit")
	public String editLeague(@FormParam("campusSession") String campusSession, 
			@PathParam("idLeague") int idLeague, 
			@FormParam("title") String title, 
			@FormParam("description") String description, 
			@FormParam("start") long start, 
			@FormParam("end") long end, 
			@FormParam("scoretype") String scoretype)
	{
		return leagueBo.editLeague(campusSession, idLeague, title, description, 
				start, end, scoretype);
	}
	
	@POST
	@Path("/league/{idLeague}/editnews")
	public String editNews(@FormParam("campusSession") String campusSession, 
			@PathParam("idLeague") int idLeague, 
			@FormParam("news") String news)
	{
		return leagueBo.editNews(campusSession, idLeague, news);
	}
	
	@POST
	@Path("/league/{id}/addcategory")
	public String addCategoryToLeague(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague, @FormParam("idCategory") int idCategory)
	{
		return leagueBo.addCategoryToLeague(campusSession, idLeague, idCategory);
	}
	
	@POST
	@Path("/league/{id}/clearcategories")
	public String clearLeagueCategories(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.clearLeagueCategories(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/addgame")
	public String addGameToPlaylist(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague, @FormParam("idGame") int idGame, @FormParam("order") int order)
	{
		return leagueBo.addGameToPlaylist(campusSession, idLeague, idGame, order);
	}
	
	@POST
	@Path("/league/{id}/clearplaylist")
	public String clearPlaylist(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.clearPlaylist(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/remove")
	public String removeLeague(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.removeLeague(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/leave")
	public String leaveLeague(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.leaveLeague(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/join")
	public String joinLeague(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.joinLeague(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/lock")
	public String lockLeague(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.lockLeague(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/unlock")
	public String unlockLeague(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.unlockLeague(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/members")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<LeagueUser> getLeagueUsers(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.getLeagueUsers(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{id}/membersinfo")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<LeagueUserInfo> getLeagueUsersInfo(@FormParam("campusSession") String campusSession, @PathParam("id") int idLeague)
	{
		return leagueBo.getLeagueUsersInfo(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{idLeague}/userteam")
	public String getUserLeagueTeam(@FormParam("campusSession") String campusSession, @PathParam("idLeague") int idLeague)
	{
		return Integer.toString(leagueBo.getUserTeam(campusSession, idLeague));
	}
	
	@POST
	@Path("/league/{idLeague}/createteam")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public String createLeagueTeam(@FormParam("campusSession") String campusSession, @PathParam("idLeague") int idLeague, @FormParam("name") String name, @FormParam("password") String password, @FormParam("byadmin") int createdByAdmin)
	{
		return leagueBo.createTeam(campusSession, idLeague, name, password, createdByAdmin);
	}
	
	@POST
	@Path("/league/{idLeague}/teams")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<LeagueTeamMembers> getLeagueTeams(@FormParam("campusSession") String campusSession, @PathParam("idLeague") int idLeague)
	{
		return leagueBo.getLeagueTeams(campusSession, idLeague);
	}
	
	@POST
	@Path("/userbysession")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public String getUserBySession(@FormParam("campusSession") String campusSession)
	{
		return Integer.toString(leagueBo.getUserBySession(campusSession));
	}
	
	@POST
	@Path("/leagueteam/{idTeam}/join")
	public String joinLeagueTeam(@FormParam("campusSession") String campusSession, @PathParam("idTeam") int idTeam, @FormParam("passwordHash") String passwordHash)
	{
		return leagueBo.joinToTeam(campusSession, idTeam, passwordHash);
	}
	
	@POST
	@Path("/leagueteam/{idTeam}/leave")
	public String leaveLeagueTeam(@FormParam("campusSession") String campusSession, @PathParam("idTeam") int idTeam)
	{
		return leagueBo.leaveTeam(campusSession, idTeam);
	}
	
	@POST
	@Path("/leagueteam/{idTeam}/remove")
	public String removeLeagueTeam(@FormParam("campusSession") String campusSession, @PathParam("idTeam") int idTeam)
	{
		return leagueBo.removeTeam(campusSession, idTeam);
	}
	
	@POST
	@Path("/leagueteam/{idTeam}/kick/{idUser}")
	public String kickUserFromTeam(@FormParam("campusSession") String campusSession, @PathParam("idTeam") int idTeam, @PathParam("idUser") int idUser)
	{
		return leagueBo.kickUserFromTeam(campusSession, idTeam, idUser);
	}
	
	@POST
	@Path("/leagueteam/{idTeam}/assign/{idUser}")
	public String assignUserToTeam(@FormParam("campusSession") String campusSession, @PathParam("idTeam") int idTeam, @PathParam("idUser") int idUser)
	{
		return leagueBo.assignUserToTeam(campusSession, idTeam, idUser);
	}
	
	@POST
	@Path("/leagueteam/{idTeam}/get")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public LeagueTeam getLeagueTeam(@FormParam("campusSession") String campusSession, @PathParam("idTeam") int idTeam)
	{
		return leagueBo.getTeam(campusSession, idTeam);
	}
	
	@POST
	@Path("/league/{idLeague}/score/single")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<LeagueUserPlaylistScore> getLeagueSingleScoretable(@FormParam("campusSession") String campusSession, @PathParam("idLeague") int idLeague)
	{
		return leagueBo.getLeagueSingleScoretable(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{idLeague}/tree/single")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<LeagueUserMatch> getLeagueSingleTree(@FormParam("campusSession") String campusSession, @PathParam("idLeague") int idLeague)
	{
		return leagueBo.getLeagueSingleTree(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{idLeague}/score/teams")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<LeagueTeamPlaylistScore> getLeagueTeamsScoretable(@FormParam("campusSession") String campusSession, @PathParam("idLeague") int idLeague)
	{
		return leagueBo.getLeagueTeamsScoretable(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{idLeague}/tree/teams")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public List<LeagueTeamMatch> getLeagueTeamsTree(@FormParam("campusSession") String campusSession, @PathParam("idLeague") int idLeague)
	{
		return leagueBo.getLeagueTeamsTree(campusSession, idLeague);
	}
	
	@POST
	@Path("/league/{idLeague}/score/submit/single/{scoreType}")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public String submitLeagueSingleScore(@FormParam("campusSession") String campusSession, @FormParam("secretGame") String uidGame, @PathParam("idLeague") int idLeague, @FormParam("score") int score, @PathParam("scoreType") String scoreType)
	{
		if(scoreType.equals("tree") || scoreType.equals("treecheck"))
		{
			return leagueBo.submitLeagueTreeSingleScore(campusSession, uidGame, idLeague, score, scoreType);
		}
		return leagueBo.submitLeagueSingleScore(campusSession, uidGame, idLeague, score, scoreType);
	}
	
	@POST
	@Path("/league/{idLeague}/score/submit/teams/{scoreType}")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public String submitLeagueTeamScore(@FormParam("campusSession") String campusSession, @FormParam("secretGame") String uidGame, @PathParam("idLeague") int idLeague, @FormParam("score") int score, @PathParam("scoreType") String scoreType)
	{
		if(scoreType.equals("tree") || scoreType.equals("treecheck"))
		{
			return leagueBo.submitLeagueTreeTeamsScore(campusSession, uidGame, idLeague, score, scoreType);
		}
		return leagueBo.submitLeagueTeamScore(campusSession, uidGame, idLeague, score, scoreType);
	}
	
	/* Others */
	@GET
	@Path("/test")
	@Produces( { MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
	public String testConnection()
	{
		System.out.println("PETICI� DE PROVA REBUDA!");
		return "OK";
	}
	
	public void setgBo(GameBO gBo) {
		this.gBo = gBo;
	}

	public GameBO getgBo() {
		return gBo;
	}

	public void setlBo(GameLikeBO lBo) {
		this.lBo = lBo;
	}

	public GameLikeBO getlBo() {
		return lBo;
	}

	public void setiBo(GameInstanceBO iBo) {
		this.iBo = iBo;
	}

	public GameInstanceBO getiBo() {
		return iBo;
	}

	public void setScBo(GameScoreBO scBo) {
		this.scBo = scBo;
	}

	public GameScoreBO getScBo() {
		return scBo;
	}


	public CategoryBO getCatBo() {
		return catBo;
	}


	public void setCatBo(CategoryBO catBo) {
		this.catBo = catBo;
	}


	public TagBO getTagBo() {
		return tagBo;
	}


	public void setTagBo(TagBO tagBo) {
		this.tagBo = tagBo;
	}


	public CommentBO getComBo() {
		return comBo;
	}


	public void setComBo(CommentBO comBo) {
		this.comBo = comBo;
	}
	
	public AchievementBO getAchBo() {
		return achBo;
	}

	public void setAchBo(AchievementBO achBo) {
		this.achBo = achBo;
	}
	
	public LeagueBO getLeagueBo() {
		return leagueBo;
	}

	public void setLeagueBo(LeagueBO lBo) {
		this.leagueBo = lBo;
	}
}
